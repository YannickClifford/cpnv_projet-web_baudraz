<?php
/**
 * Created by PhpStorm.
 * Author       :   yannick.baudraz@cpnv.ch
 * Project      :   cpnv_projet-web_baudraz - config.php
 * Description  :   Config file fot the website
 * Created      :   18.02.2019
 *
 * Updates      :   [dd.mm.yyyy author]
 *                  [description]
 * Git source   :   [link]
 */

//region errors display
// Put in comments for production
error_reporting(E_ALL);
ini_set('display_errors', true);
//endregion

//region constants
//region Website informations
define('WEBSITE_TITLE', 'SeekEasy');
define('WEBSITE_NAME', 'SeekEasy');
define('WEBSITE_URL', 'https://seekeasy.ch');
define('WEBSITE_DESCRIPTION', '');
define('WEBSITE_KEYWORDS', '');
define('WEBSITE_LANGUAGE', 'FR');
define('WEBSITE_AUTHOR', 'Yannick Baudraz');
define('WEBSITE_AUTHOR_MAIL', 'yannick.baudraz@cpnv.ch');
define('WEBSITE_ENCODING', 'UTF-8');
//endregion

//region Index actions
define('INDEX_HOME', 'index.php?action=home');
define('INDEX_SIGN_IN', 'index.php?action=sign-in');
define('INDEX_LOG_IN', 'index.php?action=log-in');
define('INDEX_SIGN_UP', 'index.php?action=sign-up');
define('INDEX_REGISTER', 'index.php?action=register');
define('INDEX_SIGN_OUT', 'index.php?action=sign-out');
define('INDEX_ADVERTS', 'index.php?action=adverts');
define('INDEX_CATEGORIES', 'index.php?action=categories');
define('INDEX_AD_DETAILS', 'index.php?action=ad-details');
define('INDEX_POST_AD', 'index.php?action=post-ad');
define('INDEX_MY_ADVERTS', 'index.php?action=my-adverts');
define('INDEX_UPDATE_ADVERT', 'index.php?action=update-advert');
define('INDEX_DELETE_ADVERT', 'index.php?action=delete-advert');
define('INDEX_FORM_MEMBER', 'index.php?action=form-member');
define('INDEX_FORM_POST_AD', 'index.php?action=form-post-ad');
define('INDEX_FORM_UPDATE_AD', 'index.php?action=form-update-ad');
//endregion

//region Paths
define('PATH_DATABASE', 'database/database.json');
define('PATH_DATABASE_CATEGORIES', 'database/categories.json');
define('PATH_DATABASE_TYPES', 'database/adverts-types.json');
//endregion
//endregion