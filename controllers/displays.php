<?php /** @noinspection PhpUnusedLocalVariableInspection */
/**
 * Created by PhpStorm.
 * Author       :   yannick.baudraz@cpnv.ch
 * Project      :   cpnv_projet-web_baudraz_app - displays.php
 * Description  :   Controller for the displaying functions
 * Created      :   29.03.2019
 *
 * Updates      :   [dd.mm.yyyy author]
 *                  [description]
 * Git source   :   [link]
 */

/**
 * Display the home page
 * Take the categories, tje adverts's types and 6 random adverts
 */
function displayHomePage()
{
  $categories = (new Categories)->getAll(PATH_DATABASE_CATEGORIES);
  $advertsTypes = (new AdvertsTypes)->getDtb(PATH_DATABASE_TYPES);
  $randomAdverts = (new Adverts)->getRandom(6);
  require_once 'views/home.php';
}

/**
 * Display the adverts page.
 * Check if there is a filter or a need to sort the adverts or neither both and take the adverts needed.
 * Get the categories and the adverts's types.
 * Paginate the adverts.
 */
function displayAdvertsPage()
{
  if (isset($_GET['category']) || isset($_GET['type']) || !empty($_GET['customWord']))
    $adverts = (new Adverts())->getFromFilter();
  elseif (!empty($_GET['sort'])) {
    switch ($_GET['sort']) {
      case 'add-date-asc':
        $order = 'ASC';
        break;
      case 'price-asc':
        $sort = 'price';
        $order = 'ASC';
        break;
      case 'price-desc':
        $sort = 'price';
        break;
    }
    $adverts = (new Adverts)->getAll($sort ?? 'addDate', $order ?? 'DESC');
  } else
    $adverts = (new Adverts)->getAll();

  $categories = (new Categories)->getAll(PATH_DATABASE_CATEGORIES);
  $advertsTypes = (new AdvertsTypes)->getDtb(PATH_DATABASE_TYPES);

  if (isset($_GET['page']))
    $page = $_GET['page'];
  $adverts = paginateArray($adverts, $page ?? 1);
  if (!empty($adverts))
    $pagination = createPaginationHTML($adverts);
  else
    $pagination = NULL;

  require_once 'views/adverts.php';
}

/**
 * Take all the categories and display the categories page.
 */
function displayCategoriesPage()
{
  $categories = (new Categories)->getAll(PATH_DATABASE_CATEGORIES);
  require_once 'views/categories.php';
}

/**
 * Display the advert page in detail.
 * Extract the advert id and the member id of the advert.
 * Take the member and the advert with the IDs.
 */
function displayAdDetailPage()
{
  $memberId = $_GET['member'];
  $advertid = $_GET['advert'];
  $member = (new Members)->getOne($memberId);
  $advert = (new Adverts)->getOne($advertid, $memberId);
  require_once 'views/ad-details.php';
}

/**
 * Display the 404 error page
 */
function displayError404Page()
{
  require_once 'views/404.php';
}

/**
 * Display the advert posting page.
 * Take all the categories and the adverts types.
 * Set a cookie that said the user is adding an advert (for Javascript).
 */
function displayPostAdPage()
{
  $categories = (new Categories)->getAll(PATH_DATABASE_CATEGORIES);
  $advertsTypes = (new AdvertsTypes)->getDtb(PATH_DATABASE_TYPES);
  setcookie('addOrUpdate', 'add');
  require_once 'views/post-ad.php';
}

/**
 * Display the updating advert page.
 * Take the advert that need to be updated.
 * Get the base 64 of the advert and the file name.
 * Take all the categories and the adverts types.
 * Set a cookie that said the user is updating an advert (for Javascript).
 */
function displayUpdateAdvert()
{
  $memberId = $_SESSION['id'];
  $advertId = $_GET['advert'];
  $advert = (new Adverts)->getOne($advertId, $memberId);
  $advert->base64 = getBase64($advert->image);
  $advert->fileName = basename($advert->image);
  $categories = (new Categories)->getAll(PATH_DATABASE_CATEGORIES);
  $advertsTypes = (new AdvertsTypes)->getDtb(PATH_DATABASE_TYPES);
  setcookie("addOrUpdate", 'update');
  require_once 'views/update-advert.php';
}

/**
 * Display the login page.
 */
function displaySignInPage()
{
  require_once 'views/sign-in.php';
}

/**
 * Display the register page.
 */
function displaySignUpPage()
{
  require_once 'views/sign-up.php';
}

/**
 * Display the account adverts page.
 * Extract the id of the member logged.
 * Take all the adverts of the member logged.
 */
function displayAccountAdvertsPage()
{
  $memberId = $_SESSION['id'];
  $adverts = (new Adverts())->getAll('addDate', 'DESC', $memberId);
  require_once 'views/my-adverts.php';
}
