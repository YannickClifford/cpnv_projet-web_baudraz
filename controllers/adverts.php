<?php
/**
 * Created by PhpStorm.
 * Author       :   yannick.baudraz@cpnv.ch
 * Project      :   cpnv_projet-web_baudraz_app - adverts.php
 * Description  :   Processes for the adverts
 * Created      :   03.03.2019
 *
 * Updates      :   [dd.mm.yyyy author]
 *                  [description]
 * Git source   :   [link]
 */

/**
 * Extract the form and the file send and give them to the model.
 * If the advert isn't posted, redirect to the posting page, else display the account adverts page.
 */
function postAdProcess()
{
  $postAdFormInputs = $_POST;
  if (!empty($_FILES['fileInput']['name']))
    $postAdFormInputs['fileInput'] = $_FILES['fileInput'];
  else
    $postAdFormInputs['fileInput'] = NULL;

  $memberId = $_SESSION['id'];
  try {
    if ((new Adverts())->postOne($postAdFormInputs, $memberId)) {
      $_GET['action'] = 'advert-posted';
      displayAccountAdvertsPage();
    } else {
      $_GET['action'] = 'post-ad-redirection';
      displayPostAdPage();
    }
  } catch (Exception $e) {
    die($e->getMessage());
  }
}

/**
 * Extract the form and the file send and give them to the model.
 * If the advert isn't updated, redirect to the updating page, else display the account adverts page.
 */
function updateAdvertProcess()
{
  $postAdFormInputs = $_POST;
  if (!empty($_FILES['fileInput']['name']))
    $postAdFormInputs['fileInput'] = $_FILES['fileInput'];
  else
    $postAdFormInputs['fileInput'] = NULL;
  $memberId = $_SESSION['id'];
  $advertId = $_GET['advert'];
  $flagImage = (new Adverts)->updateOne($postAdFormInputs, $memberId, $advertId);
  if (($flagImage === true)) {
    $_GET['action'] = 'advert-updated';
    displayAccountAdvertsPage();
  } elseif ($flagImage === 'image_exists') {
    $_GET['action'] = 'update-advert-redirection-exists';
    displayUpdateAdvert();
  } else {
    $_GET['action'] = 'update-advert-redirection';
    displayUpdateAdvert();
  }
}

/**
 * Extract the member id and the advert id to send them to the model that deleting the advert.
 * Display the account adverts page after that.
 */
function deleteAdvertProcess()
{
  $memberId = $_SESSION['id'];
  $advertId = $_GET['advert'];
  (new Adverts)->deleteOne($memberId, $advertId);
  $_GET['action'] = 'advert-deleted';
  displayAccountAdvertsPage();
}

/**
 * Check the filters for the adverts with the superglobal $_GET
 *
 * @return array
 */
function checkFiltersAdverts()
{
  $filters = [];
  if (isset($_GET['category']))
    $filters['category'] = $_GET['category'];

  if (isset($_GET['type']))
    $filters['type'] = $_GET['type'];

  if (!empty($_GET['customWord']))
    $filters['customWord'] = $_GET['customWord'];

  return $filters;
}
