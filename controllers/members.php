<?php
/**
 * Created by PhpStorm.
 * Author       :   yannick.baudraz@cpnv.ch
 * Project      :   cpnv_projet-web_baudraz_app - members.php
 * Description  :   Processes for the members
 * Created      :   02.03.2019
 *
 * Updates      :   [dd.mm.yyyy author]
 *                  [description]
 * Git source   :   [link]
 */

/**
 * Take the users infos and send it to the function that validate and send the email.
 */
function memberFormProcess()
{
  $memberId = $_GET['member'];
  $advertId = $_GET['advert'];
  $arrayOfInputs = $_POST;
  if (validateMemberForm($arrayOfInputs, $memberId, $advertId))
    $_GET['email-send'] = 'yes';
  displayAdDetailPage();
}

/**
 * Enter an member in the database
 */
function registerProcess()
{
  $registerFormData = $_POST;
  if ((new Members)->putRegister($registerFormData)) {
    $memberData['emailInput'] = $registerFormData['inputEmail'];
    $memberData['passwordInput'] = $registerFormData['inputPassword'];
    $userLogged = (new Members)->getLogin($memberData);
    $_SESSION['id'] = $userLogged->memberId;
    $_SESSION['username'] = $userLogged->completeName;
    $_SESSION['emailAddress'] = $userLogged->mail;
    $_GET['action'] = 'home';
    displayHomePage();
  } else {
    $_GET['action'] = 'sign-up-retry';
    displaySignUpPage();
  }
}

/**
 * Connect an member to the website.
 * Display the home page with success, else redirect to the login page.
 */
function loginProcess()
{
  $userData = $_POST;
  $userLogged = (new Members)->getLogin($userData);
  if ($userLogged) {
    $_SESSION['id'] = $userLogged->memberId;
    $_SESSION['username'] = $userLogged->completeName;
    $_SESSION['emailAddress'] = $userLogged->mail;
    $_GET['action'] = 'home';
    displayHomePage();
  } else {
    $_GET['action'] = 'sign-in-retry';
    displaySignInPage();
  }
}

/**
 * Destroy the session with his variables and display the home page
 */
function logout()
{
  session_unset();
  session_destroy();
  $_GET['action'] = 'home';
  displayHomePage();
}
