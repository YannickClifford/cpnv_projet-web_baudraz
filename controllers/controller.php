<?php
/**
 * Created by PhpStorm.
 * Author       :   yannick.baudraz@cpnv.ch
 * Project      :   cpnv_projet-web_baudraz - controller.php
 * Description  :   [description]
 * Created      :   19.02.2019
 *
 * Updates      :   [dd.mm.yyyy author]
 *                  [description]
 * Git source   :   [link]
 */

require_once 'models/model.php';
require_once 'controllers/displays.php';
require_once 'controllers/members.php';
require_once 'controllers/adverts.php';
