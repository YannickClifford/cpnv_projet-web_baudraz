<?php
/**
 * Created by PhpStorm.
 * Author       :   yannick.baudraz@cpnv.ch
 * Project      :   cpnv_projet-web_baudraz - index.php
 * Description  :   index/entry point of the website
 * Created      :   04.02.2019
 *
 * Updates      :   [dd.mm.yyyy author]
 *                  [description]
 * Git source   :   [link]
 */

session_start();

require_once 'config/config.php';
require_once 'vendor/autoload.php';
require_once 'controllers/controller.php';


if (isset($_GET['action'])) {
  $action = $_GET['action'];
  switch ($action) {
    case 'home':
      displayHomePage();
      break;
    case 'sign-up':
      displaySignUpPage();
      break;
    case 'register':
      registerProcess();
      break;
    case 'sign-in':
      displaySignInPage();
      break;
    case 'log-in':
      loginProcess();
      break;
    case 'sign-out':
      logout();
      break;
    case 'adverts':
      displayAdvertsPage();
      break;
    case 'categories':
      displayCategoriesPage();
      break;
    case 'ad-details':
      displayAdDetailPage();
      break;
    case 'form-member':
      memberFormProcess();
      break;
    case 'post-ad':
      if (isset($_SESSION['id']))
        displayPostAdPage();
      else {
        $_GET['action'] = 'sign-up-redirection';
        displaySignUpPage();
      }
      break;
    case 'form-post-ad':
      postAdProcess();
      break;
    case 'my-adverts':
      displayAccountAdvertsPage();
      break;
    case 'update-advert':
      displayUpdateAdvert();
      break;
    case 'form-update-ad':
      updateAdvertProcess();
      break;
    case 'delete-advert':
      deleteAdvertProcess();
      break;
    default:
      displayError404Page();
      break;
  }
} else
  displayHomePage();
