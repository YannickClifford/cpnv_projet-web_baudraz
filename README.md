# Projet web @CPNV

Site web d'annonces, avec une base de données sous forme de fichier JSON, dans le cadre de la formation d'informaticien du cpnv.

## Documentation du projet

Vous pourrez trouver toute la documentation du projet en cliquant [ici](https://bitbucket.org/YannickClifford/cpnv_projet-web_baudraz/wiki/browse/).

---

Autheur : Yannick.Baudraz@cpnv.ch

Date : Janvier à Avril 2019