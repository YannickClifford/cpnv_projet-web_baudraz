<?php
/**
 * Created by PhpStorm.
 * Author       :   yannick.baudraz@cpnv.ch
 * Project      :   cpnv_projet-web_baudraz_app - signin.php
 * Description  :   [description]
 * Created      :   04.03.2019
 *
 * Updates      :   [dd.mm.yyyy author]
 *                  [description]
 * Git source   :   [link]
 */

ob_start();
$title = "SeekEasy - Connexion";
$pageTitle = 'Connexion';
?>
  <!-- Content section Start -->
  <section id="content">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-4">
          <div class="page-login-form box">
            <h3>
              <?= $pageTitle ?>
            </h3>
            <?php if ($_GET['action'] == 'sign-in-retry'): ?>
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                Utilisateur ou mot de passe incorrect.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <?php endif; ?>
            <form role="form" class="login-form" action="<?= INDEX_LOG_IN ?>" method="post">
              <div class="form-group">
                <div class="input-icon">
                  <i class="icon fas fa-user"></i>
                  <input type="email" class="form-control" name="emailInput" placeholder="Adresse mail">
                </div>
              </div>
              <div class="form-group">
                <div class="input-icon">
                  <i class="icon fas fa-unlock-alt"></i>
                  <input type="password" class="form-control" name="passwordInput" placeholder="Mot de passe">
                </div>
              </div>
              <input type="submit" class="btn btn-common log-btn" value="Valider">
            </form>
            <div class="text-center"><a class="text-info" href="<?= INDEX_SIGN_UP ?>">S'inscrire ?</a></div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Content section End -->
<?php
$content = ob_get_clean();
require "includes/gabarit.php";
