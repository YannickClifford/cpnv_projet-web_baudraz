<?php
/**
 * Created by PhpStorm.
 * Author       :   yannick.baudraz@cpnv.ch
 * Project      :   cpnv_projet-web_baudraz_app - myadverts.php
 * Description  :   [deescription]
 * Created      :   21.03.2019
 *
 * Updates      :   [dd.mm.yyyy author]
 *                  [description]
 * Git source   :   [link]
 */

ob_start();
$title = WEBSITE_TITLE . ' - Poster une annonce';
$pageTitle = 'Mes annonces';
?>
  <div id="content">
    <div class="container">
      <div class="row">
        <?php include_once 'views/includes/sidebar-account.php' ?>
        <div class="col-lg-8 col-md-12 col-xs-12 page-content">
          <div class="inner-box">
            <?php if ($_GET['action'] == 'advert-deleted'): ?>
              <div class="alert alert-info alert-dismissible fade show" role="alert">
                Votre annonce a bien été supprimée
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <?php elseif ($_GET['action'] == 'advert-updated'): ?>
              <div class="alert alert-info alert-dismissible fade show" role="alert">
                Votre annonce a bien été modifiée
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <?php elseif ($_GET['action'] == 'advert-posted'): ?>
              <div class="alert alert-info alert-dismissible fade show" role="alert">
                Votre annonce a postée
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <?php endif; ?>
            <h2 class="title-2"><i class="fas fa-credit-card"></i> <?= $pageTitle ?></h2>
            <div class="table-responsive">
              <div class="table-action">
                <div class="table-search float-right">
                </div>
              </div>
              <table class="table table-striped table-bordered add-manage-table">
                <thead>
                  <tr>
                    <th>Image</th>
                    <th>Détails de l'annonce</th>
                    <th>Prix</th>
                    <th>Options</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($adverts as $advert): ?>
                    <?php $href = INDEX_AD_DETAILS . '&advert=' . $advert->advertId . '&member='
                                  . $advert->memberId ?>
                    <tr>
                      <td class="add-img-td">
                        <a href="<?= $href ?>">
                          <img class="photo-account-advert" src="<?= $advert->image ?>" alt="image de l'annonce">
                        </a>
                      </td>
                      <td class="ads-details-td">
                        <h4>
                          <a href="<?= $href ?>"><?= mb_convert_case(
                                $advert->title, MB_CASE_TITLE, WEBSITE_ENCODING
                            ); ?></a>
                          <span class="info">
                            <span class="category">
                              <?= mb_convert_case($advert->category, MB_CASE_TITLE, WEBSITE_ENCODING) . ' - '
                                  . mb_convert_case($advert->type, MB_CASE_TITLE, WEBSITE_ENCODING) ?>
                            </span>
                          </span>
                        </h4>

                        <div class="item_desc">
                          <a href="<?= $href ?>"
                             style="display: none"><?= upperFirst($advert->description, WEBSITE_ENCODING) ?></a>
                        </div>
                        <p><strong>Date d'ajout </strong>: <?= $advert->addDate ?></p>
                      </td>
                      <td class="price-td">
                        <strong><?= $advert->priceCurrency ?></strong>
                      </td>
                      <td class="action-td">
                        <p>
                          <a class="btn btn-primary btn-xs"
                             href="<?= INDEX_UPDATE_ADVERT . "&advert=" . $advert->advertId ?>">
                            <i class="fas fa-pencil-alt"></i> Modifier</a>
                        </p>
                        <p>
                          <a class="btn btn-danger btn-xs" data-target="#modal<?= $advert->advertId ?>"
                             data-toggle="modal">
                            <i class="fas fa-trash"></i> Supprimer
                          </a>
                        </p>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal -->
<?php foreach ($adverts as $advert): ?>
  <div class="modal fade" id="modal<?= $advert->advertId ?>" tabindex="-1" role="dialog"
       aria-labelledby="modalLabel<?= $advert->advertId ?>" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalLabel<?= $advert->advertId ?>">Confirmation</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Etes-vous sûr de vouloir supprimer cet élément ? Vous ne pourrez plus le retrouver ensuite.
        </div>
        <div class="modal-footer">
          <a class="btn btn-secondary" data-dismiss="modal">Non</a>
          <a href="<?= INDEX_DELETE_ADVERT . "&advert=" . $advert->advertId ?>"
             class="btn btn-danger">Supprimer</a>
        </div>
      </div>
    </div>
  </div>
<?php endforeach; ?>
<?php
$content = ob_get_clean();
require_once 'includes/gabarit.php';
