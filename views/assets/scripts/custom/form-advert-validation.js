'use strict'

//region JQ globals const
const helperInputJQ = $('.tg-fileuploadlabel + .invalid-feedback')
const imageOutputJQ = $('#imageOfInput')
const btnFileSpanJQ = $('.tg-fileuploadlabel > span')
const imageBoxJQ = $('.tg-fileuploadlabel')
//endregion
const requiredMessage = 'Ce champ est requis.'
const fileInputElt = document.getElementById('tg-photogallery')
let servicesOnLoad
document.onload
{
  const typeInputJQ = $('#typeInput')
  //region Image
  let file

  fileInputElt.addEventListener('change', function () {
    file = getImage(fileInputElt.files[0])
  })

  imageBoxJQ.click(function (event) {
    if (typeInputJQ[0].value === 'services')
      event.preventDefault()
  })

  typeInputJQ.change(function () {
    if (typeInputJQ[0].value === 'services') {
      imageBoxJQ.addClass('disabled')
      btnFileSpanJQ.addClass('disabled')
      fileInputElt.required = false
    } else {
      imageBoxJQ.removeClass('disabled')
      btnFileSpanJQ.removeClass('disabled')
      fileInputElt.required = true
    }
  })
  //endregion Image

  const form = document.querySelector('.needs-validation')
  form.addEventListener('submit', function (event) {
    validateFormPostAd()
    if (form.checkValidity() === false) {
      if (!(file instanceof FileReader) && typeInputJQ[0].value !== 'services') {
        if (getCookie('addOrUpdate') === 'add' || servicesOnLoad) {
          imageBoxJQ.css('borderColor', 'red')
          helperInputJQ.show()
          helperInputJQ.text(requiredMessage)
        }
      }
      event.preventDefault()
      event.stopPropagation()
    }
    form.classList.add('was-validated')
  }, false)
}

function validateFormPostAd ()
{
  //region Title
  const titleElt = document.getElementById('titleInput')
  if (titleElt.value === '')
    titleElt.setCustomValidity(requiredMessage)
  else if (titleElt.value.length <= 5)
    titleElt.setCustomValidity('Il doit y avoir au moins 5 caractères.')
  document.querySelector('#titleInput + .invalid-feedback').textContent = titleElt.validationMessage
  titleElt.setCustomValidity('')
  //endregion Title

  //region Price
  const priceElt = document.getElementById('priceInput')
  if (priceElt.validity.valueMissing && !priceElt.validity.badInput)
    priceElt.setCustomValidity(requiredMessage)
  else if (priceElt.validity.valueMissing && !priceElt.validity.badInput)
    priceElt.setCustomValidity('Cela doit être une chiffre')

  document.querySelector('#priceInput + .invalid-feedback').textContent = priceElt.validationMessage
  priceElt.setCustomValidity('')
  //endregion Price

  //region Description
  const descriptionElt = document.getElementById('descriptionInput')
  if (descriptionElt.validity.valueMissing)
    descriptionElt.setCustomValidity(requiredMessage)
  else if (descriptionElt.validity.tooShort) {
    descriptionElt.setCustomValidity(
      'La description doit être entre 10 et 500 caractères')
  }
  document.querySelector('#descriptionInput + .invalid-feedback').
    textContent = descriptionElt.validationMessage
  descriptionElt.setCustomValidity('')
  //endregion Description
}

/**
 *
 * @param file
 * @returns {FileReader|File|undefined}
 */
function getImage (file)
{
  if (file && file.type.match('image.*')) {
    const fileReader = new FileReader()
    fileReader.onload = function () {
      document.getElementById('imageOfInput').src = fileReader.result
    }
    helperInputJQ.hide()
    btnFileSpanJQ.hide()
    imageOutputJQ.show()
    imageOutputJQ.removeClass('hidden')
    if (getCookie('addOrUpdate') !== 'update')
      imageBoxJQ.css('borderColor', 'green')
    fileReader.readAsDataURL(file)
    return fileReader
  } else {
    imageOutputJQ.hide()
    btnFileSpanJQ.show()
    btnFileSpanJQ.removeClass('hidden')
    if (!file) {
      if (getCookie('addOrUpdate') !== 'update') {
        document.getElementById('imageOfInput').src = ''
        helperInputJQ.text(requiredMessage)
      }
    } else
      helperInputJQ.text('Ce n\'est pas une image.')
    helperInputJQ.show()
    btnFileSpanJQ.removeClass('hidden')
    if (getCookie('addOrUpdate') !== 'update')
      imageBoxJQ.css('borderColor', 'red')
  }

  return file
}

