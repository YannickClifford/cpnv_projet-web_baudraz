document.onload
{
  const typeInputJQ = $('#typeInput')
  const fileInputElt = document.getElementById('tg-photogallery')
  const btnFileSpanJQ = $('.tg-fileuploadlabel > span')
  const imageBoxJQ = $('.tg-fileuploadlabel')

  if (typeInputJQ[0].value === 'services') {
    imageBoxJQ.addClass('disabled')
    btnFileSpanJQ.addClass('disabled')
    servicesOnLoad = true
  } else {
    imageBoxJQ.removeClass('disabled')
    btnFileSpanJQ.removeClass('disabled')
    servicesOnLoad = false
  }
  fileInputElt.required = false

}