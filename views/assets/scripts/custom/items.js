cutDescriptionItem();

//region Items list
$('.list').click(function (e) {
  e.preventDefault();
  if (getCookie('listOrGridClass') !== 'list') {
    $('.wrapped-box-advert').unwrap();
  }
  document.cookie = "listOrGridClass=list";
});

$('.grid').click(function (e) {
  e.preventDefault();
  if (getCookie('listOrGridClass') !== 'grid') {
    $('.wrapped-box-advert').map(function () {
      if (!$(this).prev().hasClass("wrapped-box-advert")) {
        return $(this).nextUntil(":not(.wrapped-box-advert)").andSelf();
      }
    }).wrap("<div>");
  }

  document.cookie = "listOrGridClass=grid"
});
//endregion

//region Pagination
let inputToPage = $("#go-to-page-input");
let linkToPage = $('#go-to-page');
let href = linkToPage.attr('href');

function transferInputToLinkPagination(inputToPage, erase) {
  let valueInput = inputToPage.val();
  if (erase)
    linkToPage.attr('href', href.substring(0, href.length - 1));
  else
    linkToPage.attr('href', href + valueInput);
}

inputToPage.keyup(function () {
  transferInputToLinkPagination(inputToPage, false);
});
inputToPage.keydown(function () {
  transferInputToLinkPagination(inputToPage, true);
});
//endregion

function cutDescriptionItem() {
  let itemDesc = $('.item_desc > a');
  let lengthMax = 50;
  itemDesc.each(function () {
    if ($(this).text().length > lengthMax)
      $(this).text($(this).text().substring(0, lengthMax) + '...');
  });
  itemDesc.show();
}