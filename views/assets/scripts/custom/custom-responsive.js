$(function () {
  check1024Size()
  window.onload
  {
    window.addEventListener('resize', function () {
      check1024Size()
    })
  }
  function check1024Size ()
  {
    if (window.matchMedia('(max-width:1023px)').matches) {
      $('#collapseCategories').collapse('hide')
    } else {
      $('#collapseCategories').collapse('show')
    }
  }
})