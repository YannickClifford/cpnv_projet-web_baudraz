<?php
/**
 * Created by PhpStorm.
 * Author       :   yannick.baudraz@cpnv.ch
 * Project      :   cpnv_projet-web_baudraz_app - ad-details.php
 * Description  :   [deescription]
 * Created      :   05.03.2019
 *
 * Updates      :   [dd.mm.yyyy author]
 *                  [description]
 * Git source   :   [link]
 */

ob_start();
$title = "SeekEasy - Annonce";
$pageTitle = 'Annonce';
?>
  <div id="content">
    <div class="container">
      <div class="row">
        <!-- Product Info Start -->
        <div class="col-lg-8 col-md-12 col-xs-12">
          <div class="inner-box ads-details-wrapper">
            <h2><?= mb_convert_case($advert->title, MB_CASE_TITLE, WEBSITE_ENCODING) ?></h2>
            <p class="item-intro">
              <span class="poster"><?= upperFirst($advert->type, WEBSITE_ENCODING) ?> de <span
                    class="ui-bubble is-member"><?= $member->completeName ?></span>
                <span class="date"> <?= $advert->addDate ?></span>
            </p>
            <div class="item center">
              <div class="product-img">
                <a href="<?= $advert->image ?>" target="_blank"><img class="img-fluid" src="<?= $advert->image ?>"
                                                                     alt="Annonce"></a>
              </div>
              <span class="price"><?= $advert->priceCurrency ?></span>
            </div>
          </div>

          <div class="box">
            <h2 class="title-2"><strong>Détails de l'annonce</strong></h2>
            <div class="row">
              <div class="ads-details-info col-md-8">
                <p class="mb15"><?= $advert->description ?></p>
              </div>
              <div class="col-md-4">
                <aside>
                  <ul class="panel-details">
                    <li>
                      <p><strong>Prix :</strong> <?= $advert->priceCurrency ?></p>
                    </li>
                    <li>
                      <p>
                        <strong>Type :</strong>
                        <a href="<?= INDEX_ADVERTS ?>&type=<?= $advert->type ?>"><?= upperFirst(
                              $advert->type, WEBSITE_ENCODING
                          ) ?></a>
                      </p>
                    </li>
                    <li>
                      <p>
                        <strong>Catégorie :</strong>
                        <a href="<?= INDEX_ADVERTS ?>&category=<?= $advert->category ?>"><?= upperFirst(
                              $advert->category, WEBSITE_ENCODING
                          ) ?></a>
                      </p>
                    </li>
                  </ul>
                </aside>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-12 col-xs-12">
          <div class="inner-box">
            <?php if (!empty($_GET['email-send'])): ?>
              <div class="alert alert-info alert-dismissible fade show" role="alert">
                L'émail a été envoyé.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <?php endif; ?>
            <div class="widget-title">
              <h4>Annonceur</h4>
            </div>
            <div class="agent-inner">
              <div class="agent-title">
                <div class="agent-details">
                  <h6 class="mb-2">Posté par <i class="fas fa-user"></i> <?= $member->completeName ?></h6>
                  <span><i class="lnr lnr-phone-handset"></i><?= $member->phonenumber ?></span>
                </div>
              </div>
              <form action="<?= INDEX_FORM_MEMBER ?>&advert=<?= $advert->advertId ?>&member=<?= $member->memberId ?>"
                    method="post">
                <input name="nameInput" id="nameInput" type="text" class="form-control" placeholder="Votre nom*">
                <input name="emailInput" id="emailInput" type="email" class="form-control" placeholder="Votre email*">
                <div class="d-flex justify-content-end">
                  <i class="far fa-question-circle pb-1" data-toggle="tooltip" title="Pas besoin d'un format
                  spécifique pour votre numéro de téléphone."></i>
                </div>
                <input name="phoneInput" id="phoneInput" type="tel" class="form-control" placeholder="Votre téléphone*">
                <div class="d-flex justify-content-end">
                  <i class="far fa-question-circle pb-1" data-toggle="tooltip" title="Si vous ne remplissez pas ce
                  champs il prendra la forme suivante : &#148;Je suis intéréssé par votre annonce :
                  <?= $advert->title ?>&#148;"></i>
                </div>
                <textarea name="messageInput" id="messageInput" rows="5" class="form-control"
                          placeholder="Votre message"></textarea>
                <div class="d-flex justify-content-end">
                  <i class="far fa-question-circle" data-toggle="tooltip" data-html="true"
                     title="Le message sera envoyé directement par e-mail.<br>
                     Les champs comportant un &#148;*&#148; sont nécéssaires."></i>
                </div>
                <input type="submit" class="btn btn-common color-6 btn-block mt-1" value="Envoyer le message">
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Content -->
<?php
$content = ob_get_clean();
require "includes/gabarit.php";