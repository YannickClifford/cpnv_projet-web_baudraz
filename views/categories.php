<?php
/**
 * Created by PhpStorm.
 * Author       :   yannick.baudraz@cpnv.ch
 * Project      :   cpnv_projet-web_baudraz_app - categories.php
 * Description  :   [description]
 * Created      :   29.03.2019
 *
 * Updates      :   [dd.mm.yyyy author]
 *                  [description]
 * Git source   :   [link]
 */

ob_start();
$title = "SeekEasy - Categories";
$pageTitle = 'Categories';
include_once 'views/includes/grid-categories.php';
$content = ob_get_clean();
require_once "includes/gabarit.php";
