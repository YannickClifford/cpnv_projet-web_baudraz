<?php
/**
 * Created by PhpStorm.
 * Author       :   yannick.baudraz@cpnv.ch
 * Project      :   cpnv_projet-web_baudraz_app - 404.php
 * Description  :   [description]
 * Created      :   24.02.2019
 *
 * Updates      :   [dd.mm.yyyy author]
 *                  [description]
 * Git source   :   [link]
 */

ob_start();
$title = "SeekEasy - Erreur 404";
$pageTitle = '404';
?>
  <div id="content">
    <div class="container">
      <div class="row justify-content-center">
        <div class="fallback-inner">
          <form action="index.php" method="get" class="search-form">
            <div class="query-input">
              <input type="hidden" value="adverts" name="action">
              <input type="text" name="customWord" class="form-control"
                     placeholder="Essayez de rechercher une annonce ici">
            </div>
            <div class="buttons">
              <button class="btn btn-common" type="submit"><i class="fas fa-search"></i> Rechercher</button>
            </div>
          </form>
          <div class="fallback-content">
            <h2>Pour trouver le contenu manquant essayez ces étapes :</h2>
            <ul>
              <li><a rel="nofollow" href="" target="_self">Recharger cette page</a></li>
              <li>Refaite une navigation depuis la <a href="<?= INDEX_HOME ?>">page d'accueil</a>.</li>
              <li>Faite une recherche avec la barre au dessus.</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php
$content = ob_get_clean();
require "includes/gabarit.php";


