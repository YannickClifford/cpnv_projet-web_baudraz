<?php
/**
 * Created by PhpStorm.
 * Author       :   yannick.baudraz@cpnv.ch
 * Project      :   cpnv_projet-web_baudraz_app - update-advert.php
 * Description  :   [deescription]
 * Created      :   25.03.2019
 *
 * Updates      :   [dd.mm.yyyy author]
 *                  [description]
 * Git source   :   [link]
 */

ob_start();
$title = WEBSITE_TITLE . ' - Mettre à jour une annonce';
$pageTitle = 'Modifier votre annonce';
?>
  <!-- Content section Start -->
  <section id="content">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <div class="page-content">
            <div class="inner-box">
              <?php if ($_GET['action'] == 'update-advert-redirection-exists'): ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                  Le nom de l'image existe déjà.
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              <?php endif; ?>
              <h2 class="title-2">
                Détails de l'annonce
                <i class="fa fa-info-circle" data-html="true" data-placement="bottom" data-toggle="tooltip"
                   title="Les champs comportant un &#148;*&#148; sont nécéssaires."></i>
              </h2>
              <div class="dashboard-wrapper">
                <form action="<?= INDEX_FORM_UPDATE_AD . '&advert=' . $advert->advertId ?>" method="post"
                      class="needs-validation" enctype="multipart/form-data" novalidate>
                  <!--region Title-->
                  <div class="form-group mb-3">
                    <label class="control-label" for="titleInput">Titre de l'annonce*</label>
                    <input class="form-control input-md" id="titleInput" maxlength="50" minlength="5" name="titleInput"
                           placeholder="Titre de l'annonce" required type="text" value="<?= $advert->title ?>">
                    <div class="invalid-feedback"></div>
                  </div>
                  <!--endregion Title-->
                  <div class="row">
                    <!--region Category-->
                    <div class="form-group col-lg-6 mb-3">
                      <label class="control-label" for="categoryInput">Catégorie*</label>
                      <select class="custom-select" id="categoryInput" name="categoryInput" required>
                        <?php
                        foreach ($categories as $category) {
                          if ($category->text == $advert->category)
                            $selected = ' selected';
                          else
                            $selected = '';
                          echo '<option value=' . $category->text . '' . $selected . '>' . upperFirst(
                                  $category->text,
                                  WEBSITE_ENCODING
                              ) . '</option>';
                        }
                        ?>
                      </select>
                      <div class="invalid-feedback">Ce champ est requis.</div>
                    </div>
                    <!--endregion Category-->
                    <!--region Type-->
                    <div class="form-group col-lg-6 mb-3">
                      <label class="control-label" for="typeInput">Type d'annonce* </label>
                      <i class="fa fa-info-circle" data-html="true" data-placement="top" data-toggle="tooltip"
                         title="Si vous choisissez &#148;Services&#148; une image par défaut sera utilisée."></i>
                      <select class="custom-select" id="typeInput" name="typeInput" required>
                        <option disabled selected value="">Sélectionner un type</option>
                        <?php
                        foreach ($advertsTypes as $type) {
                          if ($type->text == $advert->type)
                            $selected = ' selected';
                          else
                            $selected = '';
                          echo '<option value=' . $type->text . '' . $selected . '>' . upperFirst(
                                  $type->text,
                                  WEBSITE_ENCODING
                              ) . '</option>';
                        }
                        ?>
                      </select>
                      <div class="invalid-feedback">Ce champ est requis.</div>
                    </div>
                    <!--endregion Type-->
                  </div>
                  <!--region Price-->
                  <div class="form-group mb-3">
                    <label class="control-label" for="priceInput">Prix</label>
                    <i class="fas fa-info-circle" data-html="true" data-placement="top" data-toggle="tooltip"
                       title='"A discuter" si le champ est vide, "gratuit" si vous mettez "0".'></i>
                    <input class="form-control input-md" id="priceInput" max="999999999" min="0" name="priceInput"
                           placeholder="Ex : 34.75" step="0.05" type="number" value="<?php if (!empty
                    (
                    $advert->priceForInput
                    ))
                      echo $advert->priceForInput; else echo $advert->price ?>">
                    <div class="invalid-feedback"></div>
                  </div>
                  <!--endregion Price-->
                  <!--region Description-->
                  <div class="form-group">
                    <label class="control-label" for="descriptionInput">Description*</label>
                    <textarea class="form-control" id="descriptionInput" maxlength="500" minlength="10"
                              name="descriptionInput" placeholder="Description de l'annonce" required
                              style="height: 191px;"><?= $advert->description ?></textarea>
                    <div class="invalid-feedback"></div>
                  </div>
                  <!--endregion Description-->
                  <!--region Image-->
                  <div class="form-group">
                    <label class="control-label is-invalid" for="Image">
                      Image <span data-html="true" data-placement="top" data-toggle="tooltip"
                                  title="Requis si le type n'est pas un service">(*)</span>
                    </label>
                    <i class="fas fa-info-circle" data-html="true" data-placement="top" data-toggle="tooltip"
                       title="L'image ne doit pas dépasser les 500ko.<br>Si l'image à du mal à s'afficher et que vous
                       voyez le message &#148;Image de l'annonce&#148; à la place, essayez simplement d'ajouter un autre
                       fichier. Parfois les images peuvent être corrompues et illisibles pour certains navigaeurs."></i>
                    <label class="position-relative tg-fileuploadlabel" for="tg-photogallery">
                      <span class="hidden">Image de l'annonce</span>
                      <span class="btn btn-secondary btn-post hidden">Sélectionner votre image</span>
                      <input class="tg-fileinput" id="tg-photogallery" name="fileInput" type="file"
                             accept="image/*" value="<?= $advert->fileName ?>">
                      <img alt="Image de l'annonce" id="imageOfInput" src="<?= $advert->base64 ?>">
                    </label>
                    <div class="invalid-feedback"></div>
                  </div>
                  <div class="w-100 d-flex">
                    <input class="mx-auto mt-1 btn btn-block btn-common" type="submit" value="Sauvegarder les
                    changements">
                  </div>
                  <!--endregion Image-->
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Content section End -->
<?php
$content = ob_get_clean();
require_once 'includes/gabarit.php';