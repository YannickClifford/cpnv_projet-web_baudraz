<?php
/**
 * Created by PhpStorm.
 * Author       :   yannick.baudraz@cpnv.ch
 * Project      :   cpnv_projet-web_baudraz_app - signup.php
 * Description  :   [deescription]
 * Created      :   04.03.2019
 *
 * Updates      :   [dd.mm.yyyy author]
 *                  [description]
 * Git source   :   [link]
 */

ob_start();
$title = "SeekEasy - Inscription";
$pageTitle = 'Inscription';
?>
  <!-- Content section Start -->
  <section id="content">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-6">
          <div class="page-login-form box">
            <h3>
              <?= $pageTitle ?>
              <i class="far fa-question-circle" data-html="true" data-placement="bottom" data-toggle="tooltip"
                 title="Tous les champs doivent être remplis. Passez au dessus ou appuyez sur certains icônes pour
                 plus d'informations."></i>
            </h3>
            <?php if ($_GET['action'] == 'sign-up-redirection'): ?>
              <div class="alert alert-info alert-dismissible fade show" role="alert">
                Vous avez été redirigé ici car vous avez essayé de poster une annonce sans être connecté à un compte. Si
                vous en possédez un, connectez vous en cliquant <a class="alert-link"
                                                                   href="<?= INDEX_SIGN_IN ?>">ici</a>.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <?php elseif ($_GET['action'] == 'sign-up-retry'): ?>
              <div class="alert alert-warning alert-dismissible fade show" role="alert">
                L'inscription n'a pas pu se faire, peut-être que l'utilisateur existe déjà
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <?php endif; ?>
            <!--region Form-->
            <form action="<?= INDEX_REGISTER ?>" class="login-form needs-validation" id="formRegister"
                  method="post" name="formRegister" novalidate role="form">
              <!--region Names-->
              <div class="form-row mb-4">
                <!--region Surname-->
                <div class="col-md-6">
                  <div class="input-icon">
                    <input type="text" class="form-control" name="inputSurname" placeholder="Nom de famille" required>
                    <i class="icon fas fa-user-tie"></i>
                  </div>
                </div>
                <!--endregion-->
                <!--region Firstname-->
                <div class="col-md-6">
                  <div class="input-icon">
                    <input type="text" class="form-control" name="inputFirstname" placeholder="Prénom" required>
                    <i class="icon fas fa-user"></i>
                  </div>
                </div>
                <!--endregion-->
              </div>
              <!--endregion-->
              <!--region Email address-->
              <div class="form-row mb-4">
                <div class="input-icon position-relative col">
                  <input class="form-control" id="sender-email" name="inputEmail"
                         pattern="(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)" placeholder="Adresse mail"
                         required type="email">
                  <i class="icon fas fa-envelope" data-placement="bottom" data-toggle="tooltip"
                     title="Cela doit être une adresse email."></i>
                  <div class="invalid-tooltip">Cela doit être une adresse email.</div>
                </div>
              </div>
              <!--endregion-->
              <!--region Address personal-->
              <div class="form-row mb-4 position-relative">
                <div class="input-icon col">
                  <input type="text" id="sender-email" class="form-control" name="inputAddress"
                         placeholder="Adresse postale" required>
                  <i class="icon fas fa-compass"></i>
                </div>
              </div>
              <!--endregion-->
              <!--region City-->
              <div class="form-row mb-4">
                <!--region Zipcode-->
                <div class="col-md-6">
                  <div class="input-icon position-relative">
                    <input class="form-control" max="9655" min="1000" name="inputZipcode" placeholder="Code postal"
                           required type="number">
                    <i class="icon fas fa-map-marker-alt" data-placement="bottom" data-toggle="tooltip"
                       title="Un chiffre de 1000 à 9655"></i>
                    <div class="invalid-tooltip">Un chiffre de 1000 à 9655</div>
                  </div>
                </div>
                <!--endregion-->
                <!--region City name-->
                <div class="col-md-6">
                  <div class="input-icon">
                    <input type="text" class="form-control" name="inputCity" placeholder="Localité" required>
                    <i class="icon fas fa-map"></i>
                  </div>
                </div>
                <!--endregion-->
              </div>
              <!--endregion -->
              <!--region Phone number-->
              <div class="form-row  mb-4">
                <div class="input-icon position-relative col">
                  <input type="tel" class="form-control" name="inputPhone" placeholder="Téléphone"
                         pattern="^[0-9]{10}$" required>
                  <i class="icon fas fa-phone" data-placement="bottom" data-toggle="tooltip"
                     title="Format : 0121231212"></i>
                  <div class="invalid-tooltip">Format : 0121231212</div>
                </div>
              </div>
              <!--endregion -->
              <!--region Passwords-->
              <div class="form-row mb-4">
                <div class="form-group col-md-6">
                  <div class="input-icon position-relative">
                    <input class="form-control" id="inputPassword" name="inputPassword"
                           pattern="[A-Za-z0-9!#$%&'()*+,-./:;<=>?@[\]^_`{|}~]{6,}" placeholder="Mot de passe" required
                           type="password">
                    <i class="icon fas fa-unlock" data-placement="bottom" data-toggle="tooltip"
                       title="Au moins 6 caractères. Caractères acceptés : A->Z;a->z;0->9;@-_."></i>
                    <div class="invalid-tooltip">Au moins 6 caractères. Caractères acceptés : A->Z;a->z;0->9;@-_.</div>
                  </div>
                </div>

                <div class="form-group col-md-6">
                  <div class="input-icon position-relative">
                    <input class="form-control" id="inputConfirmPassword" name="inputConfirmPassword"
                           placeholder="Confirmer" required type="password">
                    <i class="icon fas fa-lock" data-placement="bottom" data-toggle="tooltip"
                       title="Les mots de passe doivent être identiques."></i>
                    <div class="invalid-tooltip">Les mots de passe doivent être identiques.</div>
                  </div>
                </div>
              </div>
              <!--endregion-->
              <!--region Checkbox-->
              <div class="form-group custom-control custom-checkbox center">
                <input class="custom-control-input" type="checkbox" id="conditions" name="inputConditions" required>
                <label class="custom-control-label" for="conditions">Je confirme que toutes ces données sont
                  correctes</label>
              </div>
              <!--endregion-->
              <!--region Submit-->
              <input type="submit" class="btn btn-common log-btn" value="S'inscrire">
              <!--endregion-->
            </form>
            <!--endregion From-->
          </div>
        </div>
      </div>
    </div>
  </section>
  <script>
    let password = document.getElementById('inputPassword'),
      confirmPassword = document.getElementById('inputConfirmPassword')

    window.onload
    {
      'use strict'
      window.addEventListener('load', function () {
        password.onchange = validatePassword
        confirmPassword.onkeyup = validatePassword
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        const form = document.querySelector('.needs-validation')
        // Loop over them and prevent submission
        form.addEventListener('submit', function (event) {
          if (form.checkValidity() === false) {
            event.preventDefault()
            event.stopPropagation()
          }
          form.classList.add('was-validated')
        }, false)
      }, false)
    }

    function validatePassword ()
    {
      if (password.value !== confirmPassword.value) {
        confirmPassword.setCustomValidity('Les mots de passes doivent être identiques')
      } else {
        confirmPassword.setCustomValidity('')
      }
    }
  </script>
<?php
$content = ob_get_clean();
require_once 'includes/gabarit.php';