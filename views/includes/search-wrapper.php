<!-- Search wrapper Start -->
<div id="search-row-wrapper"
    <?php if (isset($_GET['action']) && $_GET['action'] != 'home' && $_GET['action'] != 'log-in'
              &&
    $_GET['action'] != 'sign-out') : ?>
     style="background: url('views/assets/images/background/counter-bg.jpg') center center no-repeat;">
  <?php endif; ?>
  <div class="container">
    <div class="search-inner<?php if (!isset($_GET['action']) || $_GET['action'] == 'home')
      echo ' mt-0' ?>">
      <!-- Start Search box -->
      <div class="search-bar row shadow-sm">
        <fieldset>
          <form action="index.php" method="get" class="search-form">
            <input type="hidden" value="adverts" name="action">
            <div class="form-group">
              <i class="lni-bullhorn"></i>
              <input type="text" name="customWord" class="form-control" placeholder="Que recherchez-vous ?">
            </div>
            <div class="form-group">
              <div class="tg-select">
                <!--suppress HtmlFormInputWithoutLabel -->
                <select name="type">
                  <option disabled selected value="">Type</option>
                  <?php foreach ($advertsTypes as $type) : ?>
                    <option value=<?= $type->name ?>><?= upperFirst($type->text, 'UTF-8') ?></option>
                  <?php endforeach; ?>
                  <option value="">Aucun</option>
                </select>
                <i class="fas fa-angle-down"></i>
              </div>
            </div>
            <div class="form-group">
              <div class="tg-select">
                <!--suppress HtmlFormInputWithoutLabel -->
                <select name="category">
                  <option disabled selected value="">Catégorie</option>
                  <?php foreach ($categories as $category): ?>
                    <option value=<?= $category->name ?>>
                      <?= mb_convert_case($category->text, MB_CASE_TITLE, 'UTF-8') ?>
                    </option>
                  <?php endforeach; ?>
                  <option value="">Aucune</option>
                </select>
                <i class="fas fa-angle-down"></i>
              </div>
            </div>
            <a href="#">
              <button class="btn btn-common" type="submit"><i class="fas fa-search"></i> Rechercher</button>
            </a>
          </form>
        </fieldset>
      </div>
      <!-- End Search box -->
    </div>
  </div>
</div>
<!-- Search wrapper End -->
