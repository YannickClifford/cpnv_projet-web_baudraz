<!-- Footer Section Start -->
<footer>
  <!-- Footer Area Start -->
  <section class="footer-Content">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-6 col-xs-12 wow fadeIn" data-wow-delay="0.2s">
          <div class="widget">
            <h3 class="block-title">À Propos de nous</h3>
            <div class="textwidget">
              <p>Nous proposons des annonces en tout genre depuis 2016.</p>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 col-xs-12 wow fadeIn text-center" data-wow-delay="0.4s">
          <div class="widget">
            <h3 class="block-title">Liens utiles</h3>
            <ul class="menu">
              <li><a href="<?= INDEX_HOME ?>">Accueil</a></li>
              <li><a href="<?= INDEX_ADVERTS ?>">Annonces</a></li>
              <li><a href="<?= INDEX_SIGN_IN ?>">Connexion</a></li>
              <li><a href="<?= INDEX_SIGN_UP ?>">Inscription</a></li>
            </ul>
          </div>
        </div>

        <div class="col-lg-4  d-flex wow fadeIn p-3" data-wow-delay="0.6s">
          <img class="align-self-center w-25 h-auto mx-auto" src="/views/assets/images/logo/logo-bgd.png"
               alt="Logo SeakEasy" id="footer-logo">
        </div>
      </div>
    </div>
  </section>
  <!-- Footer area End -->

  <!-- Copyright Start  -->
  <div id="copyright">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="site-info float-left">
            <p>Developed by <?= WEBSITE_AUTHOR ?>.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Copyright End -->
</footer>
<!-- Footer Section End -->