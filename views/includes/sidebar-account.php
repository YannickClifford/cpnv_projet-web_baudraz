<?php
$activeMyAccount = (isset($_GET['action']) && $_GET['action'] == 'my-account');
$activeMyAdverts = (isset($_GET['action'])
                    && ($_GET['action'] == 'my-adverts'
                        || $_GET['action'] == 'advert-posted'
                        || $_GET['action'] == 'advert-deleted'
                        || $_GET['action'] == 'advert-updated'));
?>
<div class="col-xs-12 col-md-12 col-lg-4 page-sidebar">
  <aside>
    <div class="sidebar-box">
      <div class="user">
        <div class="usercontent">
          <h3><?= $_SESSION['username'] ?></h3>
        </div>
        <figure>
          <a href="#"><img src="views/assets/images/icons/gender-neutral-user.png" alt="Account user"></a>
        </figure>
      </div>
      <nav class="navdashboard">
        <ul>
          <li>
            <a <?php if ($activeMyAdverts): ?>class="active"<?php endif ?> href="<?= INDEX_MY_ADVERTS ?>">
              <i class="fas fa-ad"></i>
              <span>Mes annonces</span>
            </a>
          </li>
          <li>
            <a href=<?= INDEX_SIGN_OUT ?>>
              <i class="fas fa-times"></i>
              <span>Se déconnecter</span>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </aside>
</div>
