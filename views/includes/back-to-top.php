<?php
/**
 * Created by PhpStorm.
 * Author       :   yannick.baudraz@cpnv.ch
 * Project      :   cpnv_projet-web_baudraz - back-to-top.php
 * Description  :   [deescription]
 * Created      :   18.02.2019
 *
 * Updates      :   [dd.mm.yyyy author]
 *                  [description]
 * Git source   :   [link]
 */
?>

<!-- Go To Top Link -->
<a href="#" class="back-to-top">
    <i class="fas fa-angle-up"></i>
</a>
