<?php
/**
 * Created by PhpStorm.
 * Author       :   yannick.baudraz@cpnv.ch
 * Project      :   cpnv_projet-web_baudraz_app - carousel.php
 * Description  :   [deescription]
 * Created      :   28.02.2019
 *
 * Updates      :   [dd.mm.yyyy author]
 *                  [description]
 * Git source   :   [link]
 */
?>

<!--region Carousel-->
<div id="carousel-area">
  <div id="carousel-slider" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carousel-slider" data-slide-to="0" class="active"></li>
      <li data-target="#carousel-slider" data-slide-to="1"></li>
      <li data-target="#carousel-slider" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img class="d-block w-100" src="views/assets/images/slider/slide1.jpg" alt="">
        <div class="carousel-caption text-left">
          <h2 class="fadeInLeft wow" data-sppb-wow-delay="1.2s">
            Bienvenue sur <?= WEBSITE_NAME ?>
          </h2>
          <p class="fadeInLeft wow" data-sppb-wow-delay="1.2s">La Recherche Facile Sans Se Prendre La Tête.</p>
          <a class="btn btn-lg btn-common fadeInLeft wow" data-sppb-wow-delay="1.2s" href="<?= INDEX_SIGN_UP ?>">
            Rejoignez-Nous !
          </a>
        </div>
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="views/assets/images/slider/slide2.jpg" alt="">
        <div class="carousel-caption text-left">
          <h2 class="fadeInUp wow" data-sppb-wow-delay="1.2s">
            Vendez et Achetez Tout Ce Que Vous Voulez !
          </h2>
          <p class="fadeInUp wow" data-sppb-wow-delay="1.2s">Vous Avez Sûrement Quelque Chose À Vendre<br>
            N'hésitez Plus !</p>
          <a class="btn btn-lg btn-common fadeInUp wow" data-sppb-wow-delay="1.2s" href="<?= INDEX_POST_AD ?>">
            Poster Une Annonce
          </a>
        </div>
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="views/assets/images/slider/slide3.jpg" alt="">
        <div class="carousel-caption text-center">
          <h2 class="fadeInDown wow" data-sppb-wow-delay="1.2s">
            Découvrez Des Offres Uniques
          </h2>
          <p class="fadeInDown wow" data-sppb-wow-delay="1.2s">
            Voitures Neuves ou d'Occasion, Smartphones, Ordinateurs,<br>
            Jobs, Propriétés Et Encore Plus !
          </p>
          <a class="btn btn-lg btn-common fadeInDown wow" data-sppb-wow-delay="1.2s" href="<?= INDEX_ADVERTS ?>">
            Consulter Les Annonces
          </a>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carousel-slider" role="button" data-slide="prev">
      <span class="carousel-control fa fa-angle-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carousel-slider" role="button" data-slide="next">
      <span class="carousel-control fa fa-angle-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
<!--endregion Carousel-->
