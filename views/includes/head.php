<!-- Meta Tags -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta name="author" content="<?= WEBSITE_AUTHOR ?>">
<meta name="description" content="<?= WEBSITE_DESCRIPTION ?>">
<meta name="keywords" content="<?= WEBSITE_KEYWORDS ?>">
<meta name="Reply-to" content="<?= WEBSITE_AUTHOR_MAIL ?>">
<meta name="Copyright" content="<?= WEBSITE_AUTHOR ?>">
<meta name="Language" content="<?= WEBSITE_LANGUAGE ?>">

<!-- CSS Styles -->
<!-- Favicon -->
<link rel="shortcut icon" href="/views/assets/images/logo/favicon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="/views/assets/styles/css/bootstrap.min.css" type="text/css">
<!-- Font Awesome CSS -->
<link rel="stylesheet" href="/views/assets/fonts/fontawesome/css/all.min.css" type="text/css">
<!-- Line Icons CSS -->
<link rel="stylesheet" href="/views/assets/fonts/line-icons/line-icons.css" type="text/css">
<!-- Animate CSS -->
<link rel="stylesheet" href="/views/assets/styles/extras/animate.css" type="text/css">
<!-- Owl Carousel -->
<link rel="stylesheet" href="/views/assets/styles/extras/owl.carousel.css" type="text/css">
<link rel="stylesheet" href="/views/assets/styles/extras/owl.theme.css" type="text/css">
<!-- Slicknav js -->
<link rel="stylesheet" href="/views/assets/styles/css/slicknav.min.css" type="text/css">
<!-- Main Styles -->
<link rel="stylesheet" href="/views/assets/styles/css/main.css" type="text/css">
<!-- Responsive CSS Styles -->
<link rel="stylesheet" href="/views/assets/styles/css/responsive.css" type="text/css">
<!-- Own Styles -->
<link rel="stylesheet" href="/views/assets/styles/css/custom.css" type="text/css">
<link rel="stylesheet" href="/views/assets/styles/css/purple.css" type="text/css">
