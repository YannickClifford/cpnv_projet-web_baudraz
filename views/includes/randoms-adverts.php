<section class="featured-lis">
  <div class="container">
    <div class="row">
      <div class="col-12 wow fadeIn" data-wow-delay="0.8s">
        <h3 class="section-title">Quelques annonces</h3>
        <div id="new-products" class="owl-carousel">
          <?php foreach ($randomAdverts as $advert): ?>
            <?php $href = INDEX_AD_DETAILS . '&advert=' . $advert->advertId . '&member=' . $advert->memberId ?>
            <div class="owl-item" style="width: 100%">
              <div class="product-item">
                <div class="carousel-thumb" style="height: 125px">
                  <img src="<?= $advert->image ?>" alt="">
                  <div class="overlay">
                    <a href="<?= $href ?>"><i class="fas fa-link"></i></a>
                  </div>
                </div>
                <a href="ads-details.html" class="item-name"><?= $advert->title ?></a>
                <span class="price"><?= $advert->priceCurrency ?></span>
              </div>
            </div>
          <?php endforeach ?>
        </div>
      </div>
    </div>
  </div>
</section>