<?php
if (isset($_GET['action']))
  $action = $_GET['action'];
?>
<!doctype html>
<html lang="fr">
<head>
  <?php include_once 'head.php'; ?>
  <title><?= $title; ?></title>
</head>
<body>
  <?php include_once 'header.php';

  if (isset($_GET['action'])
      && $_GET['action'] != 'home'
      && $_GET['action'] != 'log-in'
      && $_GET['action'] != 'sign-out'
      && $_GET['action'] != 'adverts')
    include 'page-header.php';

  if (!isset($_GET['action']) || $_GET['action'] == 'home')
    include 'views/includes/carousel.php';

  if (!isset($_GET['action']) || $_GET['action'] == 'home' || $_GET['action'] == 'adverts')
    include 'views/includes/search-wrapper.php';

  echo $content;

  include_once 'footer.php';

  include_once 'back-to-top.php';

  include_once 'post-add-responsive.php';

  if ((!isset($_GET['action'])
       || $_GET['action'] == 'home'
       || $_GET['action'] == 'log-in'
       || $_GET['action'] == 'logout'
       || $_GET['action'] == 'sign-in'
       || $_GET['action'] == 'sign-up'
       || $_GET['action'] == 'post-ad'
       || $_GET['action'] == 'adverts'
       || $_GET['action'] == 'categories'
       || $_GET['action'] == 'my-adverts'))
    include_once 'start-loader.php';
  ?>
</body>
</html>

<?php include_once 'js-loader.php' ?>
