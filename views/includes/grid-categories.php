<div class="container">
  <div class="row">
    <div class="wrapper">
      <section class="grid-category">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="box-title no-border">
                <div class="inner">
                  <h3 class="section-title">Catégories</h3>
                  <a class="sell-your-item" href="<?= INDEX_ADVERTS ?>"><i class="fas fa-th-large"></i> Toutes les
                    annonces</a>
                </div>
              </div>
            </div>
            <?php $counter = 0;
            foreach ($categories as $category) : ?>
              <div class="col-lg-2 col-md-3 col-xs-12 f-category wow fadeIn" data-wow-delay="<?= $counter ?>s">
                <a href="<?= INDEX_ADVERTS . '&category=' . $category->name ?>">
                  <div class="icon-img">
                    <img src="<?= $category->image ?>" class="img-fluid" alt="image">
                  </div>
                  <h6><?= mb_strtoupper($category->text, "UTF-8") ?></h6>
                </a>
              </div>
              <?php $counter += 0.2;
            endforeach; ?>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>
