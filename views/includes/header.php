<?php
$categories = (new Categories)->getAll(PATH_DATABASE_CATEGORIES);
$advertsTypes = (new AdvertsTypes)->getDtb(PATH_DATABASE_TYPES);
$activeAdverts = (isset($_GET['action']) && $_GET['action'] == 'adverts');
$activeCategory = (isset($_GET['action']) && $_GET['action'] == 'categories');
$activeSignIn = (isset($_GET['action']) && ($_GET['action'] == 'sign-in' || $_GET['action'] == 'sign-in-retry'));
$activeSignUp = (isset($_GET['action'])
                 && ($_GET['action'] == 'sign-up'
                     || $_GET['action'] == 'sign-up-redirection'
                     || $_GET['action'] == 'sign-up-retry'));
$activeAccount = (isset($_GET['action'])
                  && ($_GET['action'] == 'my-account'
                      || $_GET['action'] == 'my-adverts'
                      || $_GET['action'] == 'advert-deleted'));
$activeMyAdverts = (isset($_GET['action'])
                    && ($_GET['action'] == 'my-adverts'
                        || $_GET['action'] == 'advert-posted'
                        || $_GET['action'] == 'advert-deleted'
                        || $_GET['action'] == 'advert-updated'));
?>
<!-- Header Section Start -->
<div class="header">
  <!-- Navbar Start -->
  <nav class="navbar navbar-expand-lg bg-inverse fixed-top scrolling-navbar">
    <div class="container">
      <div class="theme-header clearfix">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header h-100">
          <button class="navbar-toggler" type="button" data-toggle="collapse"
                  aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
          </button>
          <a href=<?= INDEX_HOME ?>><img class="navbar-brand"
                                         src="views/assets/images/logo/logo_transparent_alt.png"
                                         alt="Logo SeekEasy"></a>
        </div>

        <div class="collapse navbar-collapse" id="main-navbar">
          <ul class="navbar-nav mr-auto w-100 justify-content-end">
            <li class="nav-item">
              <a class="nav-link <?php if (!isset($_GET['action']) || $_GET['action'] == 'home') : ?>active<?php
              endif ?>" href=<?= INDEX_HOME ?>>
                Home
              </a>
            </li>

            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle<?php if ($activeAdverts): ?> active<?php endif ?>"
                 href=<?= INDEX_ADVERTS
              ?>>Annonces</a>
              <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="<?= INDEX_ADVERTS ?>" id="all-adverts">Toutes les annonces</a></li>
                <?php foreach ($advertsTypes as $type) : ?>
                  <li>
                    <a class="dropdown-item<?php if (isset($_GET['type'])
                                                     && $_GET['type'] == $type->name): ?> active<?php endif ?>"
                       href=<?= INDEX_ADVERTS . '&type=' . $type->name ?>>
                      <?= upperFirst($type->text, 'UTF-8'); ?>
                    </a>
                  </li>
                <?php endforeach; ?>
              </ul>

            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle<?php if ($activeCategory): ?> active<?php endif ?>"
                 href=<?= INDEX_CATEGORIES ?>>Catégories</a>
              <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="<?= INDEX_CATEGORIES ?>" id="all-categories">Toutes les
                    catégories</a></li>
                <?php foreach ($categories as $category) : ?>
                  <li>
                    <a class="dropdown-item<?php if (isset($_GET['category'])
                                                     && $_GET['category'] == $category->name): ?> active<?php endif ?>"
                       href=<?= INDEX_ADVERTS . '&category=' . $category->name ?>>
                      <?= mb_convert_case($category->text, MB_CASE_TITLE, 'UTF-8'); ?>
                    </a>
                  </li>
                <?php endforeach ?>
              </ul>
            </li>

            <?php if (isset($_SESSION['id'])): ?>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle<?php if ($activeAccount): ?> active<?php endif ?>" href="#"
                   id="nav-member">
                  <i class="fa fa-user"></i> <?= $_SESSION['username'] ?>
                </a>
                <ul class="dropdown-menu">
                  <li>
                    <span class="dropdown-header">Compte de <?= $_SESSION['username'] ?></span>
                    <div class="dropdown-divider my-0"></div>
                  </li>
                  <li>
                    <a class="dropdown-item<?php if ($activeMyAdverts): ?> active<?php endif ?>"
                       href=<?= INDEX_MY_ADVERTS ?>>
                      <i class="fa fa-ad"></i> Mes annonces
                    </a>
                  </li>
                  <li>
                    <a class="dropdown-item" href=<?= INDEX_SIGN_OUT ?>><i class="fa fa-sign-out-alt"></i> Se
                      déconnecter</a>
                  </li>
                </ul>
              </li>

              <li class="nav-item">
                <a class="nav-link" data-toggle="tooltip" title="Déconnexion"
                   href=<?= INDEX_SIGN_OUT ?>><i class="fa fa-sign-out-alt"></i></a>
              </li>
            <?php else: ?>
              <li class="nav-item">
                <a class="nav-link<?php if ($activeSignIn) : ?> active<?php endif ?>" href=<?= INDEX_SIGN_IN ?>>
                  <i class="fas fa-sign-in-alt"></i> Connexion
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link<?php if ($activeSignUp) : ?> active<?php endif ?>" href=<?= INDEX_SIGN_UP ?>>
                  <i class="fas fa-file-signature"></i> Inscription
                </a>
              </li>
            <?php endif ?>

            <li class="postadd">
              <a class="btn btn-danger btn-post w-auto" href="<?= INDEX_POST_AD ?>"><span
                    class="fas fa-plus-circle"></span> Poster</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="mobile-menu" data-logo="assets/img/logo-mobile.png"></div>
  </nav>
  <!-- Navbar End -->
</div>
<!-- Header Section End -->