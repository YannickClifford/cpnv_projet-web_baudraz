<!-- Main JS  -->
<script type="text/javascript" src="views/assets/scripts/jquery-min.js"></script>
<script type="text/javascript" src="views/assets/scripts/popper.min.js"></script>
<script type="text/javascript" src="views/assets/scripts/bootstrap.min.js"></script>
<script type="text/javascript" src="views/assets/scripts/jquery.parallax.js"></script>
<script type="text/javascript" src="views/assets/scripts/owl.carousel.min.js"></script>
<script type="text/javascript" src="views/assets/scripts/jquery.slicknav.js"></script>
<script type="text/javascript" src="views/assets/scripts/wow.js"></script>
<script type="text/javascript" src="views/assets/scripts/jquery.counterup.min.js"></script>
<script type="text/javascript" src="views/assets/scripts/waypoints.min.js"></script>
<script type="text/javascript" src="views/assets/scripts/main.js"></script>
<script type="text/javascript" src="views/assets/scripts/custom/custom.js"></script>
<script type="text/javascript" src="views/assets/scripts/custom/custom-responsive.js"></script>
<?php if (isset($_GET['action']) && ($_GET['action'] == 'adverts' || $_GET['action'] == 'my-adverts'))
  echo '<script type="text/javascript" src="views/assets/scripts/custom/items.js"></script>' ?>
<?php if (isset($_GET['action'])
          && ($_GET['action'] == 'post-ad'
              || $_GET['action'] == 'post-ad-redirection'
              || $_GET['action'] == 'update-advert'
              || $_GET['action'] == 'update-advert-redirection'))
  echo '<script type="text/javascript" src="views/assets/scripts/custom/form-advert-validation.js"></script>' ?>
<?php if (isset($_GET['action'])
          && ($_GET['action'] == 'update-advert' || $_GET['action'] == 'update-advert-redirection'))
  echo '<script type="text/javascript" src="views/assets/scripts/custom/form-update-advert.js"></script>' ?>
<script>
  $('.dropdown-menu a.dropdown-toggle').on('click', function () {
    if (!$(this).next().hasClass('show')) {
      $(this).parents('.dropdown-menu').first().find('.show').removeClass('show')
    }
    var $subMenu = $(this).next('.dropdown-menu')
    $subMenu.toggleClass('show')

    $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function () {
      $('.dropdown-submenu .show').removeClass('show')
    })

    return false
  })
</script>

