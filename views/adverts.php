<?php
/**
 * Created by PhpStorm.
 * Author       :   yannick.baudraz@cpnv.ch
 * Project      :   cpnv_projet-web_baudraz_app - adverts.php
 * Description  :   [description]
 * Created      :   27.02.2019
 *
 * Updates      :   [dd.mm.yyyy author]
 *                  [description]
 * Git source   :   [link]
 */

ob_start();
$title = WEBSITE_TITLE . ' - Annonces';
?>
  <!--region Main container -->
  <div class="main-container">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-md-12 col-xs-12 page-sidebar">
          <!--region Aside categories-->
          <aside>
            <div class="inner-box">
              <div class="categories">
                <a href="#collapseCategories" data-toggle="collapse" aria-controls="collapseCategories"
                   aria-expanded="true">
                  <div class="widget-title">
                    <i class="fas fa-align-justify mr-1"></i>
                    <h4>Catégories</h4>
                    <i class="fa fa-chevron-down ml-1"></i>
                  </div>
                </a>
                <div class="categories-list collapse show" id="collapseCategories">
                  <ul>
                    <?php foreach ($categories as $category) : ?>
                      <li>
                        <a href="<?= INDEX_ADVERTS . '&category=' . $category->name; ?>">
                          <?= upperFirst($category->text, "UTF-8"); ?>
                        </a>
                      </li>
                    <?php endforeach; ?>
                  </ul>
                </div>
              </div>
            </div>
          </aside>
          <!--endregion Aside categories-->
        </div>

        <div class="col-lg-9 col-md-12 col-xs-12 page-content">
          <!--region Product filter -->
          <div class="product-filter">
            <div class="grid-list-count">
              <a class="list switchToGrid <?php if (isset($_COOKIE['listOrGridClass'])
                                                    && $_COOKIE['listOrGridClass'] == 'list')
                echo 'active'; ?>"
                 href="#" data-toggle="tooltip" title="Afficher par liste"><i class="fas fa-list"></i></a>
              <a class="grid switchToList <?php if (isset($_COOKIE['listOrGridClass'])
                                                    && $_COOKIE['listOrGridClass'] == 'grid')
                echo 'active'; ?>"
                 href="#" data-toggle="tooltip" title="Afficher par grille"><i class="fas fa-th-large"></i></a>
            </div>
            <div class="short-name">
              <form class="name-ordering" method="post">
                <div class="form-group">
                  <label>
                    <select name="order" class="form-control" onchange="location = this.value;">
                      <option selected disabled value="">Trier par</option>
                      <option value="<?= INDEX_ADVERTS ?>">Date d'ajout: Plus récent</option>
                      <option value="<?= INDEX_ADVERTS . "&sort=add-date-asc" ?> ">Date d'ajout: Plus ancien</option>
                      <option value="<?= INDEX_ADVERTS . "&sort=price-asc" ?>">Prix: Moins élevé</option>
                      <option value="<?= INDEX_ADVERTS . "&sort=price-desc" ?>">Prix: Plus élevé</option>
                    </select>
                  </label>
                </div>
              </form>
            </div>
          </div>
          <!--endregion Product filter -->

          <!--region Adds wrapper -->
          <div class="adds-wrapper mt-2">
            <?php foreach ($adverts as $advert): ?>
              <?php $href = INDEX_AD_DETAILS . '&advert=' . $advert->advertId . '&member=' . $advert->memberId ?>
              <?php
              if (isset($_COOKIE['listOrGridClass'])) {
                if ($_COOKIE['listOrGridClass'] == 'list')
                  echo '<div class="item-list make-list">';
                elseif ($_COOKIE['listOrGridClass'] == 'grid')
                  echo '<div class="item-list make-grid">';
              } else
                echo '<div class="item-list">';
              ?>
              <div class="flex-advert">
                <?php if (isset($_COOKIE['listOrGridClass']) && $_COOKIE['listOrGridClass'] == 'grid')
                  echo '<div class="w-100">'; ?>
                <div class="col-sm-2 no-padding photobox my-auto wrapped-box-advert">
                  <div class="add-image d-flex justify-content-center">
                    <a href="<?= $href ?>"><img class="photo-advert" src="<?= $advert->image ?>" alt="Annonce"></a>
                  </div>
                </div>
                <div class="col-sm-7 add-desc-box wrapped-box-advert">
                  <div class="add-details">
                    <h5 class="add-title">
                      <a href="<?= $href ?>">
                        <?= mb_convert_case($advert->title, MB_CASE_TITLE, WEBSITE_ENCODING) ?>
                      </a>
                    </h5>
                    <div class="info">
                      <span class="date">
                        <i class="fas fa-clock"></i>
                        <?= $advert->addDate ?>
                      </span><br>
                      <span class="category">
                        <?= mb_convert_case($advert->category, MB_CASE_TITLE, WEBSITE_ENCODING) . ' - '
                            . mb_convert_case($advert->type, MB_CASE_TITLE, WEBSITE_ENCODING) ?></span>
                    </div>
                    <div class="item_desc">
                      <a href="<?= $href ?>"
                         style="display: none"><?= upperFirst($advert->description, WEBSITE_ENCODING) ?></a>
                    </div>
                  </div>
                </div>
                <?php
                if (isset($_COOKIE['listOrGridClass']) && $_COOKIE['listOrGridClass'] == 'grid')
                  echo '</div>';
                ?>

                <div class="col-sm float-right price-box my-auto">
                  <h2 class="item-price text-center"><?= $advert->priceCurrency ?></h2>
                  <a href="<?= $href ?>" class="btn btn-common color-6 btn-block">Voir l'annonce</a>
                </div>
              </div>
              <?= '</div>'; ?>
            <?php endforeach; ?>
            <!--endregion Adds wrapper -->
          </div>
          <!-- region Pagination -->
          <div class="col-12">
            <?php if (!empty($advert)): ?>
              <?= $pagination ?>
            <?php else: ?>
              <div class="alert alert-info" role="alert">
                <h4 class="alert-heading">Hmm</h4>
                <p>Il semblerait n'y avoir aucune annonces...</p>
                <hr>
                <p class="mb-0">Peut être avez-vous fait une recherche qui n'a retourné aucun résultat. Cliquez <a
                      class="alert-link" href="<?= INDEX_ADVERTS ?>"><u>ici</u></a> pour voir toutes les annonces.</p>
              </div>
            <?php endif; ?>
          </div>
          <!-- endregion Pagination -->
        </div>
      </div>
    </div>
  </div>
  <!--endregion Main container -->
<?php
$content = ob_get_clean();
require_once 'includes/gabarit.php';