<?php
/**
 * Created by PhpStorm.
 * Author       :   yannick.baudraz@cpnv.ch
 * Project      :   cpnv_projet-web_baudraz - categories.php
 * Description  :   [description]
 * Created      :   18.02.2019
 *
 * Updates      :   [dd.mm.yyyy author]
 *                  [description]
 * Git source   :   [link]
 */

ob_start();
$title = "SeekEasy - Acceuil";
include 'views/includes/grid-categories.php';
include 'views/includes/randoms-adverts.php';
$content = ob_get_clean();
require "includes/gabarit.php";


