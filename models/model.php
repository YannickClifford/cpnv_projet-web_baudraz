<?php
/**
 * Created by PhpStorm.
 * Author       :   yannick.baudraz@cpnv.ch
 * Project      :   cpnv_projet-web_baudraz_app - model.php
 * Description  :   [description]
 * Created      :   26.02.2019
 *
 * Updates      :   [dd.mm.yyyy author]
 *                  [description]
 * Git source   :   [link]
 */

require_once 'models/helpers/functions/functions.php';
require_once 'models/classes/Categories.php';
require_once 'models/classes/Members.php';
require_once 'models/classes/Adverts.php';
require_once 'models/pagination.php';
require_once 'models/form-validation.php';

/**
 * Create the directoryPath of images
 *
 * @param $data
 * @param $imagesPath
 * @return mixed $data
 */
function createImagesPath($data, $imagesPath)
{
  if (!is_object($data)) {
    foreach ($data as $datum)
      if (!isset($datum->image))
        foreach ($datum->adverts as $advert)
          $advert->image = setFullPath($imagesPath, $advert->image);
      else
        $datum->image = setFullPath($imagesPath, $datum->image);
  } else
    $data->image = setFullPath($imagesPath, $data->image);

  return $data;
}

/**
 * Translate the category name for the categoryInEnglish.
 * Expected to be an categoryInEnglish and his category name to match with one of the names in the switch.
 * It's used for the services images, who need a generic image who is their image category.
 *
 * @param object $advert
 * @return string
 */
function translateCategoriesName($advert)
{
  $categories = (new Categories)->getAll(PATH_DATABASE_CATEGORIES);

  foreach ($categories as $category) {
    if ($category->text == $advert->category) {
      $advert->image = $category->name;
    }
  }

  return $advert;
}

/**
 * Format for displaying in billion, million and thousand
 *
 * @param $numberWithCurrency
 *
 * @return string
 */
function formatBigNumber($numberWithCurrency)
{
  $number  =  preg_replace('/\s+/u', '', $numberWithCurrency);
  $lengthNumber = strlen($numberWithCurrency);
  $currency = substr($numberWithCurrency, $lengthNumber - 3, $lengthNumber);
  $number = substr($number, 0, $lengthNumber - 5);
  $number = str_replace(',', '.', $number);
  $number = floatval($number);

  if ($number < 1000)
    $format = number_format($number, 2);
  elseif ($number < 1000000) {
    // Anything less than a million
    $format = number_format($number / 1000, 2) . 'K';
  } else if ($number < 1000000000) {
    // Anything less than a billion
    $format = number_format($number / 1000000, 2) . 'M';
  } else {
    // At least a billion
    $format = number_format($number / 1000000000, 2) . 'B';
  }

  $format = $format . ' ' . $currency;

  return $format;
}