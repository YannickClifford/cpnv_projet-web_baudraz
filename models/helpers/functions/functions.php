<?php
/**
 * Created by PhpStorm.
 * Author       :   yannick.baudraz@cpnv.ch
 * Project      :   cpnv_projet-web_baudraz_app - functions.php
 * Description  :   [deescription]
 * Created      :   10.03.2019
 *
 * Updates      :   [dd.mm.yyyy author]
 *                  [description]
 * Git source   :   [link]
 */

/**
 * This function is designed to append a directoryPath with the fileName received as parameter.
 * The directoryPath will be found by the function.
 *
 * @param string $path  The directoryPath to be appends to the file name
 * @param string $fName The file name to be append to the directoryPath
 *
 * @return string       Full directoryPath to the log file expressed as a string
 * @example File Name : testFile.log / after function : [pathToFile]\testFile.log
 */
function setFullPath($path, $fName)
{
  $fullPathToFile = $path . "/" . $fName;

  return $fullPathToFile;
}

/**
 * Make upper the first character of a string with an encoding
 *
 * @param string $string
 * @param string $encoding
 *
 * @return string
 * @example $string = "électronique catégorie", $encoding = "UTF-8" -> return "Électronique
 *          catégorie"
 */
function upperFirst($string, $encoding)
{
  $strlen = mb_strlen($string, $encoding);
  $firstChar = mb_substr($string, 0, 1, $encoding);
  $then = mb_substr($string, 1, $strlen - 1, $encoding);

  return mb_strtoupper($firstChar, $encoding) . $then;
}

/**
 * Sort a multi-dimensional array of objects by key value
 * Usage: usort($array, arrSortObjsByKey('VALUE_TO_SORT_BY'));
 * Expects an array of objects.
 *
 * @param String $key   The name of the parameter to sort by
 * @param String $order The sort order
 * @return Closure      A function to compare using usort
 */
function arrSortObjsByKey($key, $order = 'DESC')
{
  /**
   * Anonymous function
   *
   * @param object $a
   * @param object $b
   * @return int
   */
  return function ($a, $b) use ($key, $order) {
    // Swap order if necessary
    if ($order == 'DESC')
      list($a, $b) = [$b, $a];

    // Check data type
    return is_numeric($a->{$key}) ? $a->{$key} > $b->{$key} : strnatcasecmp($a->{$key}, $b->{$key});
  };
}

/**
 * Arrange a number to the CHF format currency
 *
 * @param $number
 * @return float
 */
function arrangePrice($number)
{
  $number = floatval($number);
  $format = new NumberFormatter('fr_CH.utf8', NumberFormatter::CURRENCY);
  $number = $format->formatCurrency($number, 'CHF');

  return $number;
}

/**
 * Check if the parameter is an email
 *
 * @param $email
 * @return false|mixed
 */
function isEmail($email)
{
  return filter_var($email, FILTER_VALIDATE_EMAIL);
}

/**
 * check if the parameter is a phone number
 *
 * @param $phone
 * @return false|int
 */
function isPhone($phone)
{
  return preg_match("/^[0-9]*$/", $phone);
}

/**
 * Clean the inputs and prevent for attacks like javascript injections
 *
 * @param $datum
 * @return string
 */
function cleanInput($datum)
{
  $datum = trim($datum);
  $datum = stripslashes($datum);
  $datum = htmlspecialchars($datum);

  return $datum;
}

/**
 * Check if it's a jpg/png image and if the size isn't more than 500ko
 *
 * @param $image
 * @return mixed
 */
function checkImage($image)
{
  $imageFlag = true;
  $check = exif_imagetype($image["tmp_name"]);

  switch ($check) {
    case IMAGETYPE_JPEG:
    case IMAGETYPE_PNG:
      breaK;
    default:
      $imageFlag = false;
  }

  if ($image['size'] > 500000)
    $imageFlag = false;

  return $imageFlag;
}

/**
 * Get the base 64 of an image file
 *
 * @param $imagePath
 * @return string
 */
function getBase64($imagePath)
{
  $type = pathinfo($imagePath, PATHINFO_EXTENSION);
  $data = file_get_contents($imagePath);
  $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

  return $base64;
}
