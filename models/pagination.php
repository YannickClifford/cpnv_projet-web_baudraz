<?php
/**
 * Created by PhpStorm.
 * Author       :   yannick.baudraz@cpnv.ch
 * Project      :   cpnv_projet-web_baudraz_app - pagination.php
 * Description  :   [description]
 * Created      :   04.03.2019
 *
 * Updates      :   [dd.mm.yyyy author]
 *                  [description]
 * Git source   :   [link]
 */

use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;

/**
 * Paginate the adverts
 *
 * @param $array
 * @param $currentPage
 * @return Pagerfanta
 */
function paginateArray($array, $currentPage)
{
  $adapter = new ArrayAdapter($array);
  $pagerFanta = new Pagerfanta($adapter);
  $pagerFanta->setMaxPerPage(4);
  if ($currentPage > $pagerFanta->getNbPages())
    $currentPage = $pagerFanta->getNbPages();
  elseif ($currentPage < 1)
    $currentPage = 1;
  $pagerFanta->setCurrentPage($currentPage ?? 1);

  return $pagerFanta;
}

/**
 * Create the HTML structure of the pagination
 *
 * @param $pagerFanta
 * @return false|string
 */
function createPaginationHTML(Pagerfanta $pagerFanta)
{
  //region Functions
  /**
   * HTML for the first page
   *
   * @param string $url
   */
  function firstPage($url)
  {
    echo '<li class="page-item hidden-responsive"><a class="page-link" href="' . $url . 1 . '">Première</a></li>';
  }

  /**
   * HTML for the previous page
   *
   * @param string $url
   * @param int    $currentPage
   */
  function previousPage($url, $currentPage)
  {
    echo '<li class="page-item"><a class="page-link" href="' . $url . ($currentPage - 1) . '">Précédente</a></li>';
  }

  /**
   * HTML for the next page
   *
   * @param string $url
   * @param int    $currentPage
   */
  function nextPage($url, $currentPage)
  {
    echo '<li class="page-item"><a class="page-link" href="' . $url . ($currentPage + 1) . '">Suivante</a></li>';
  }

  /**
   * HTML for the next page
   *
   * @param string $url
   * @param int    $lastPage
   */
  function lastPage($url, $lastPage)
  {
    echo '<li class="page-item hidden-responsive"><a class="page-link" href="' . $url . ($lastPage)
         . '">Dernière</a></li>';
  }

  /**
   * HTML for the page needed according to the situation
   *
   * @param string $url
   * @param int    $currentPage
   * @param int    $i
   */
  function pageNeeded($url, $currentPage, $i)
  {
    echo '<li class="page-item">';
    if ($i == $currentPage)
      echo '<a class="page-link active disabled">' . $i . '</a>';
    else
      echo '<a class="page-link hidden-responsive" href="' . $url . $i . '">' . $i . '</a>';
    echo '</li>';
  }
  //endregion

  ob_start();
  $currentPage = $pagerFanta->getCurrentPage();
  $lastPage = $pagerFanta->getNbPages();
  $actualUrl = $_SERVER['REQUEST_URI'];
  $urlAdd = '';
  $urlLength = strlen($actualUrl);

  if (!isset($_GET['page'])) {
    $urlAdd = '&page=';
    $subUrl = substr($actualUrl, 0);
  } elseif ($currentPage >= 10 && $currentPage < 100)
    $subUrl = substr($actualUrl, 0, $urlLength - 2);
  else
    $subUrl = substr($actualUrl, 0, $urlLength - 1);
  $url = $subUrl . $urlAdd;
  ?>
  <div class="row pagination-bar d-flex justify-content-center">
    <nav>
      <ul class="pagination">
        <?php
        if ($pagerFanta->hasPreviousPage()) {
          firstPage($url);
          previousPage($url, $currentPage);
        }
        switch (true) {
          case ($currentPage <= 5 && $lastPage < 10):
            for ($i = 1; $i <= $lastPage; $i++)
              pageNeeded($url, $currentPage, $i);
            break;
          case (!$pagerFanta->hasPreviousPage() || ($pagerFanta->hasPreviousPage() && $currentPage <= 5)):
            for ($i = 1; $i <= 10; $i++)
              pageNeeded($url, $currentPage, $i);
            break;
          case ($currentPage > 5 && $currentPage > $lastPage - 5):
            for ($i = ($currentPage - 5); $i <= $lastPage; $i++)
              pageNeeded($url, $currentPage, $i);
            break;
          case ($currentPage > 5 && $currentPage <= ($lastPage - 5)):
            for ($i = ($currentPage - 5); $i < ($currentPage + 5); $i++)
              pageNeeded($url, $currentPage, $i);
            break;
          case (($currentPage > ($lastPage - 5) && $pagerFanta->hasNextPage()) || !$pagerFanta->hasNextPage()):
            for ($i = ($lastPage - 9); $i <= $lastPage; $i++)
              pageNeeded($url, $currentPage, $i);
            break;
        }
        if ($pagerFanta->hasNextPage()) {
          nextPage($url, $currentPage);
          lastPage($url, $lastPage);
        }
        ?>
      </ul>
    </nav>
  </div>
  <div class="row d-flex justify-content-center">
    <!--suppress HtmlFormInputWithoutLabel -->
    <input id="go-to-page-input" class="form-control col col-lg-3" type="text" width="100" value=""
           placeholder="Aller à la page...">
    <a id="go-to-page" href="<?= $url ?>" class="btn btn-primary col-2 ml-2 position-relative" style="top: 5px;">
      Go !
    </a>
  </div>
  <?php
  $paginationHTML = ob_get_clean();

  return $paginationHTML;
}