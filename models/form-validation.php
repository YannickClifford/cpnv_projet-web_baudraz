<?php
/**
 * Created by PhpStorm.
 * Author       :   yannick.baudraz@cpnv.ch
 * Project      :   cpnv_projet-web_baudraz_app - form-member-validation.php
 * Description  :   [deescription]
 * Created      :   11.03.2019
 *
 * Updates      :   [dd.mm.yyyy author]
 *                  [description]
 * Git source   :   [link]
 */

/**
 * Clean the inputs and send a mail to the email of the advert's member
 *
 * @param array  $arrayOfInputs
 * @param string $memberId
 * @param string $advertId
 * @return bool
 */
function validateMemberForm($arrayOfInputs, $memberId, $advertId)
{
  $member = (new Members)->getOne($memberId);
  $emailTo = $member->mail;
  $advert = (new Adverts)->getOne($advertId, $memberId);
  $advertTitle = $advert->title;

  foreach ($arrayOfInputs as $index => $input)
    $arrayOfInputs[$index] = cleanInput($input);

  if ($arrayOfInputs['messageInput'] === '')
    $arrayOfInputs['messageInput'] = utf8_decode('Je suis intéressé par votre annonce : ' . $advert->title);

  $emailText = "";
  $emailText .= "Bonjour,\n\n{$arrayOfInputs['messageInput']}\n\n\n";
  $emailText .= "Nom : {$arrayOfInputs['nameInput']}\n";
  $emailText .= "E-mail : {$arrayOfInputs['emailInput']}\n";
  $emailText .= "Téléphone : {$arrayOfInputs['phoneInput']}\n";
  $subject = "Un message pour votre annonce : {$advertTitle}";
  $headers = "From: {$arrayOfInputs['nameInput']}" . "\r\n" .
             "Reply-To: {$arrayOfInputs['emailInput']}" . "\r\n";
  $mail = mail($emailTo, utf8_decode($subject), utf8_decode($emailText), utf8_decode($headers));
  return $mail;
}