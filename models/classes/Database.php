<?php
/**
 * Created by PhpStorm.
 * Author       :   yannick.baudraz@cpnv.ch
 * Project      :   cpnv_projet-web_baudraz_app - Database.php
 * Description  :   [description]
 * Created      :   02.03.2019
 *
 * Updates      :   [dd.mm.yyyy author]
 *                  [description]
 * Git source   :   [link]
 */

/**
 * Class Database
 */
class Database
{
  public $data;

  /**
   * Get all the database from a JSON file and return it in a array of objects
   *
   * @param $dtbFullPath
   * @return mixed
   */
  public function getDtb($dtbFullPath)
  {
    if (file_exists($dtbFullPath))
      $this->data = json_decode(file_get_contents($dtbFullPath));

    return $this->data;
  }
}