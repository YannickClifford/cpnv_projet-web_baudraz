<?php
/**
 * Created by PhpStorm.
 * Author       :   yannick.baudraz@cpnv.ch
 * Project      :   cpnv_projet-web_baudraz_app - AdvertsTypes.php
 * Description  :   [description]
 * Created      :   27.02.2019
 *
 * Updates      :   [dd.mm.yyyy author]
 *                  [description]
 * Git source   :   [link]
 */

require_once 'models/classes/Database.php';

/**
 * Class AdvertsTypes
 */
class AdvertsTypes extends Database
{
  /**
   * Get one category from a JSON file
   *
   * @param $dtbFullPath
   * @param $dataSpecified
   * @return mixed
   */
  public function getOne($dtbFullPath, $dataSpecified)
  {
    $oneCategory = null;
    $this->getDtb($dtbFullPath);

    foreach ($this->data as $datum) {
      if ($datum->name == $dataSpecified)
        $oneCategory = $datum;
    }

    return $oneCategory;
  }
}