<?php
/**
 * Created by PhpStorm.
 * Author       :   yannick.baudraz@cpnv.ch
 * Project      :   cpnv_projet-web_baudraz_app - Categories.php
 * Description  :   Category class
 * Created      :   26.02.2019
 *
 * Updates      :   [dd.mm.yyyy author]
 *                  [description]
 * Git source   :   [link]
 */

require_once 'models/classes/AdvertsTypes.php';

/**
 * Class Categories
 */
class Categories extends AdvertsTypes
{
  private const IMAGES_PATH = 'views/assets/images/categories';

  /**
   * Get all the categories from a JSON file
   *
   * @param $dtbFullPath
   * @return mixed
   */
  public function getAll($dtbFullPath)
  {
    $this->getDtb($dtbFullPath);
    $this->data = createImagesPath($this->data, self::IMAGES_PATH);

    return $this->data;
  }

  /**
   * Get one category from a JSON file
   *
   * @param $dtbFullPath
   * @param $dataSpecified
   * @return mixed
   */
  public function getOne($dtbFullPath, $dataSpecified)
  {
    $this->data = parent::getOne($dtbFullPath, $dataSpecified);
    $this->data = createImagesPath($this->data, self::IMAGES_PATH);

    return $this->data;
  }
}