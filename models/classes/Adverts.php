<?php
/**
 * Created by PhpStorm.
 * Author       :   yannick.baudraz@cpnv.ch
 * Project      :   cpnv_projet-web_baudraz_app - Adverts.php
 * Description  :   [description]
 * Created      :   02.03.2019
 *
 * Updates      :   [dd.mm.yyyy author]
 *                  [description]
 * Git source   :   [link]
 */

require_once 'models/classes/Database.php';

/**
 * Class Adverts
 */
class Adverts extends Database
{

  private const IMAGES_PATH = 'views/assets/images/items';

  /**
   * Get all the adverts from a JSON file.
   * If $memberId is not null, it will take all the adverts from the member with the ID
   *
   * @param string          $dtbFullPath
   * @param string          $keyToSort
   * @param string          $order
   * @param null|string|int $memberId
   * @return mixed
   */
  public function getAll($keyToSort = 'addDate', $order = 'DESC', $memberId = NULL)
  {
    $this->getDtb(PATH_DATABASE);
    $this->data = createImagesPath($this->data, self::IMAGES_PATH);

    $advertsArray = [];
    foreach ($this->data as $member) {
      if ($memberId != NULL) {
        if ($member->memberId == $memberId) {
          foreach ($member->adverts as $advert) {
            $advert->memberId = $member->memberId;
            $this->managePrice($advert);
            if ($advert->type == 'services')
              $this->data = $this->chgImgServices($advert);
            array_push($advertsArray, $advert);
          }
        }
      } else {
        foreach ($member->adverts as $advert) {
          $advert->memberId = $member->memberId;
          $this->managePrice($advert);
          if ($advert->type == 'services')
            $this->data = $this->chgImgServices($advert);
          array_push($advertsArray, $advert);
        }
      }
    }
    $this->data = $advertsArray;
    usort($this->data, arrSortObjsByKey($keyToSort, $order));

    return $this->data;
  }

  /**
   * Get random adverts from a JSON file
   *
   * @param $nbOfAdverts
   * @return mixed
   */
  public function getRandom($nbOfAdverts)
  {
    $this->getAll();
    $result = [];
    if (!empty($this->data)) {
      if ($nbOfAdverts > count($this->data))
        $nbOfAdverts = count($this->data);
      foreach (array_rand($this->data, $nbOfAdverts) as $advert)
        $result[] = $this->data[$advert];
    }


    return $result;
  }

  /**
   * Get one advert from a JSON file
   *
   * @param $advertId
   * @param $memberId
   * @return mixed|null
   */
  public function getOne($advertId, $memberId)
  {
    $this->getDtb(PATH_DATABASE);
    $this->data = createImagesPath($this->data, self::IMAGES_PATH);

    foreach ($this->data as $member) {
      if ($member->memberId == $memberId) {
        foreach ($member->adverts as $advert) {
          if ($advert->advertId == $advertId) {
            $this->data = $advert;
            $this->managePrice($advert);
            if ($advert->type == 'services')
              $this->data = $this->chgImgServices($advert);
          }
        }
      }
    }

    return $this->data;
  }

  /**
   * Post an advert for the user logged in the website, from a form.
   *
   * @param $formData
   * @param $memberId
   * @return bool
   * @throws \Exception - Throws an exception if the new Datetime in the function catch an error
   */
  public function postOne($formData, $memberId)
  {
    $this->getDtb(PATH_DATABASE);
    $flagImage = false;
    $file = $formData['fileInput'];
    $targetDir = 'views/assets/images/items/';
    $targetFile = $targetDir . basename($file["name"]);

    if (!$file || (checkImage($file) && !file_exists($targetFile))) {
      $formData['titleInput'] = cleanInput($formData['titleInput']);
      $formData['descriptionInput'] = cleanInput($formData['descriptionInput']);
      foreach ($this->data as $member) {
        if ($member->memberId == $memberId) {
          $date = new DateTime('now');
          $dateNow = $date->format('Y-m-d H:i:s');
          $lastAdvertIndex = end($member->adverts);
          if (isset($lastAdvertIndex->advertId))
            $lastAdvert = $lastAdvertIndex->advertId;
          else
            $lastAdvert = 0;

          if ($formData['priceInput'] === "")
            $formData['priceInput'] = 'à discuter';
          elseif ($formData['priceInput'] === '0')
            $formData['priceInput'] = 'gratuit';

          $advertArray = [
              'advertId' => $lastAdvert + 1,
              'title' => $formData["titleInput"],
              'type' => $formData["typeInput"],
              'category' => $formData["categoryInput"],
              'price' => $formData["priceInput"],
              'image' => $file["name"],
              'description' => $formData["descriptionInput"],
              'addDate' => $dateNow,
          ];

          array_push($member->adverts, $advertArray);
          move_uploaded_file($file['tmp_name'], $targetFile);
          $flagImage = true;
        }
      }
    }
    file_put_contents(PATH_DATABASE, json_encode($this->data), LOCK_EX);

    return $flagImage;
  }

  /**
   * Update an advert
   *
   * @param $formData
   * @param $memberId
   * @param $advertId
   * @return bool
   */
  public function updateOne($formData, $memberId, $advertId)
  {
    $this->getDtb(PATH_DATABASE);
    $flagImage = NULL;
    $file = $formData['fileInput'];
    $targetDir = 'views/assets/images/items/';
    $targetFile = $targetDir . basename($file["name"]);

    if (!$file || (checkImage($file) && !file_exists($targetFile))) {
      foreach ($this->data as $indexM => $member) {
        if ($member->memberId == $memberId) {
          foreach ($member->adverts as $indexA => $advert) {
            if ($advert->advertId == $advertId) {
              $advertTemp = $advert;
              if ($formData['priceInput'] === "")
                $formData['priceInput'] = 'à discuter';
              elseif ($formData['priceInput'] === '0')
                $formData['priceInput'] = 'gratuit';
              // Delete the old image if the advert pass to "not a service" to "services"
              if ($advert->type != 'services' && $formData['typeInput'] == 'services') {
                $imagePath = setFullPath(self::IMAGES_PATH, $advert->image);
                unlink($imagePath);
              }
              $advertTemp->title = $formData['titleInput'];
              $advertTemp->type = $formData['typeInput'];
              $advertTemp->category = $formData['categoryInput'];
              if (is_numeric($formData['priceInput']))
                $advertTemp->price = number_format($formData['priceInput'], 2);
              else
                $advertTemp->price = $formData['priceInput'];
              $advertTemp->description = $formData['descriptionInput'];
              // Don't update if there is no file and the type isn't a service
              if (!(!$file && $advert->type != 'services'))
                $advertTemp->image = $file["name"];
              $this->data[$indexM]->adverts[$indexA] = $advertTemp;
              move_uploaded_file($file['tmp_name'], $targetFile);
              $flagImage = true;
            }
          }
        }
      }
      file_put_contents(PATH_DATABASE, json_encode($this->data), LOCK_EX);
    } elseif (file_exists($targetFile))
      $flagImage = 'image_exists';

    return $flagImage;
  }

  /**
   * Delete an advert
   *
   * @param $memberId
   * @param $advertId
   */
  public function deleteOne($memberId, $advertId)
  {
    $this->getDtb(PATH_DATABASE);
    foreach ($this->data as $indexM => $member) {
      if ($member->memberId == $memberId) {
        foreach ($member->adverts as $indexA => $advert) {
          if ($advert->advertId == $advertId) {
            $imagePath = setFullPath(self::IMAGES_PATH, $advert->image);
            unset($this->data[$indexM]->adverts[$indexA]);
            $this->data[$indexM]->adverts = array_values($this->data[$indexM]->adverts);
            if ($advert->type != 'services')
              unlink($imagePath);
          }
        }
      }
    }
    //    $arrayJson = array_values($this->data);
    $json = json_encode($this->data);
    file_put_contents(PATH_DATABASE, $json);
  }

  /**
   * Get adverts from a JSON file with a filter.
   * The filter except to be a category or an advert's type
   * The filter needs to be the "text" field in his file to match
   *
   * @example For the category "others" the text field is "autres"
   * @param string $dtbFullPath
   * @return mixed
   */
  public function getFromFilter()
  {
    $filtersArr = checkFiltersAdverts();
    $this->getAll();
    //region Custom Word
    if (isset($filtersArr['customWord'])) {
      foreach ($this->data as $index => $advert) {
        $searchInTitle = stripos($advert->title, $filtersArr['customWord']);
        $searchInDescription = stripos($advert->description, $filtersArr['customWord']);
        if ($searchInTitle === false && $searchInDescription === false)
          unset($this->data[$index]);
      }
    }
    //endregion
    //region Category
    $filterCategory = NULL;
    if (!empty($filtersArr['category']))
      $filterCategory = (new Categories)->getOne(PATH_DATABASE_CATEGORIES, $filtersArr['category']);
    $isCategoryInGet = (!empty($filtersArr['category']));
    //endregion
    //region Type
    $filterType = NULL;
    if (!empty($filtersArr['type']))
      $filterType = (new AdvertsTypes)->getOne(PATH_DATABASE_TYPES, $filtersArr['type']);
    $isTypeInGet = (!empty($filtersArr['type']));
    //endregion
    $isTwoFiltersInGet = ($isCategoryInGet && $isTypeInGet);
    foreach ($this->data as $index => $advert) {
      $filterCategoryCond = ($isCategoryInGet && $advert->category != $filterCategory->text);
      $filterTypeCond = ($isTypeInGet && $advert->type != $filterType->text);
      $twoFiltersCond = ($isTwoFiltersInGet
                         && ($advert->category != $filterCategory->text || $advert->type != $filterType->text));
      if ($filterCategoryCond || $filterTypeCond || $twoFiltersCond)
        unset($this->data[$index]);
    }

    return $this->data;
  }

  /**
   * Arrange the price of the advert
   *
   * @param object $advert
   */
  private function managePrice($advert)
  {
    if ($advert->price != 'à discuter' && $advert->price != 'gratuit') {
      $advert->priceCurrency = arrangePrice($advert->price);
      $advert->priceCurrency = formatBigNumber($advert->priceCurrency);
    } else {
      if ($advert->price == 'à discuter')
        $advert->priceForInput = $advert->price;
      elseif ($advert->price == 'gratuit')
        $advert->priceForInput = 0;
      $advert->priceCurrency = upperFirst($advert->price, WEBSITE_ENCODING);
      $advert->price = 0;
    }
  }

  /**
   * Translate the category name for the advert.
   * It's used for the services images, who need a generic image who is their image category.
   *
   * @param object $advert
   *
   * @return string
   */
  private function chgImgServices($advert)
  {
    $categories = (new Categories)->getAll(PATH_DATABASE_CATEGORIES);
    foreach ($categories as $category)
      if ($category->text == $advert->category)
        $advert->image = $category->image;

    return $advert;
  }
}