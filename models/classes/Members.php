<?php
/**
 * Created by PhpStorm.
 * Author       :   yannick.baudraz@cpnv.ch
 * Project      :   cpnv_projet-web_baudraz_app - Members.php
 * Description  :   [description]
 * Created      :   02.03.2019
 *
 * Updates      :   [dd.mm.yyyy author]
 *                  [description]
 * Git source   :   [link]
 */

require_once 'models/classes/Database.php';

/**
 * Class Members
 */
class Members extends Database
{

  /**
   * Get one member from a JSON file
   *
   * @param $dtbFullPath
   * @param $dataSpecified
   * @param $isIdEmail
   *
   * @return mixed
   */
  public function getOne($dataSpecified, $isIdEmail = false)
  {
    $oneMember = NULL;
    $this->getDtb(PATH_DATABASE);

    foreach ($this->data as $datum) {
      if ($isIdEmail) {
        if ($datum->mail == $dataSpecified)
          $oneMember = $datum;
      } elseif ($datum->memberId == $dataSpecified)
        $oneMember = $datum;
    }
    $this->createUserName($oneMember);

    return $oneMember;
  }

  /**
   * Check if the data are corrects fot the login and take the user infos
   *
   * @param $dtbFullPath
   * @param $userData
   *
   * @return object|null
   */
  public function getLogin($userData)
  {
    $userLogged = NULL;

    $this->getDtb(PATH_DATABASE);

    foreach ($this->data as $member) {
      if ($member->mail == $userData['emailInput'])
        if (password_verify($userData['passwordInput'], $member->password)) {
          $userLogged = $member;
          $this->createUserName($userLogged);
        }
    }

    return $userLogged;
  }

  /**
   * Insert a member in a JSON file
   *
   * @param $dtbFullPath
   * @param $userData
   * @return bool
   */
  public function putRegister($userData)
  {
    $registerFlag = false;
    $memberExists = false;
    if (($userData['inputPassword'] === $userData['inputConfirmPassword']) && $userData['inputConditions'] === 'on') {
      $this->getDtb(PATH_DATABASE);

      foreach ($this->data as $member)
        if ($member->mail == $userData['inputEmail'])
          $memberExists = true;

      if (!$memberExists) {
        if (!empty($this->data)) {
          $lastMember = end($this->data);
          $lastId = $lastMember->memberId;
        } else
          $lastId = 0;
        foreach ($userData as $index => $userDatum) {
          $userData[$index] = cleanInput($userDatum);
          if ($index !== 'inputPassword')
            $userData[$index] = mb_strtolower($userData[$index], WEBSITE_ENCODING);
        }
        $userArrayTemp = [
            'memberId' => $lastId + 1,
            'name' => $userData['inputSurname'],
            'firstname' => $userData['inputFirstname'],
            'address' => $userData['inputAddress'],
            'zipcode' => $userData['inputZipcode'],
            'location' => $userData['inputCity'],
            'phonenumber' => "+41" . substr($userData['inputPhone'], 1),
            'mail' => $userData['inputEmail'],
            'password' => password_hash($userData['inputPassword'], PASSWORD_DEFAULT),
            'adverts' => [],
        ];
        $newMember = (object) $userArrayTemp;
        array_push($this->data, $newMember);
        file_put_contents(PATH_DATABASE, json_encode($this->data), LOCK_EX);
        $registerFlag = true;
      }
    }

    return $registerFlag;
  }

  /**
   * Create the complete name of the member
   * Complete name is first name + name
   *
   * @example $member->firstname : John, $member->name : Doe => $member->completeName = John Doe
   * @param $member
   */
  private function createUserName($member)
  {
    $member->completeName = mb_convert_case($member->firstname . ' ' . $member->name, MB_CASE_TITLE, WEBSITE_ENCODING);
  }
}
